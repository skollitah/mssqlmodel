EXEC dbm.db_model_utl_createProc 'db_model_drop_tables_columns'
GO

ALTER PROCEDURE dbm.db_model_drop_tables_columns
(
	@schname sysname,
	@oname sysname = NULL,
	@cname sysname = NULL
)
AS BEGIN

/*	
BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_tables_columns 'dbo', 'RequiredAdditionalDocument', 'CustomerType_id'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_tables_columns 'dbo', 'RequiredAdditionalDocument'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_tables_columns 'dbo', NULL, 'CustomerType_id'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_tables_columns 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_get_tables_columns
*/
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF
	
	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1, 1),
		[schname] sysname,
		[oname] sysname NOT NULL,
		[kname] sysname NULL
	)

	IF @cname IS NULL
	BEGIN
		INSERT INTO #db_objects
		(
			[schname],
			[oname]
		)
			SELECT
				DISTINCT
				G.[schname],
				G.oname
			FROM	dbm.db_model_get_tables_columns AS G
			WHERE
				(
				G.oname = @oname
				OR @oname IS NULL
				)
				AND G.schname = @schname
	END
	ELSE IF @cname IS NOT NULL
	BEGIN
		INSERT INTO #db_objects
		(
			[schname],
			[oname],
			[kname]
		)
			SELECT
				DISTINCT
				G.[schname],
				G.oname,
				G.cname
			FROM	dbm.db_model_get_tables_columns AS G
			WHERE
				G.oname = @oname
				AND G.cname = @cname
				AND G.schname = @schname
	END

	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @ikname sysname
	DECLARE @sql varchar(1000)

	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			O.[schname],
			O.[oname],
			O.[kname]
		FROM	#db_objects AS O
	
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @ikname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @ikname IS NULL
		BEGIN
			SET @sql = ('DROP TABLE [' + @ischname + '].[' + @ioname + ']')
		END
		ELSE
		BEGIN
			SET @sql = ('ALTER TABLE [' + @ischname + '].[' + @ioname + '] DROP COLUMN [' +@ikname + ']')
		END
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @ikname
	END

	CLOSE dropObjects
	DEALLOCATE dropObjects	
END
