EXEC dbm.db_model_utl_createView 'db_model_get_rules'
GO

ALTER VIEW dbm.db_model_get_rules
AS
-- select * from dbm.db_model_get_rules
	SELECT
		RO.[name] as [rname],
		T.name AS [oname],
		CONVERT(sysname, NULL) as [cname],
		SC.[definition] as [rtext],
		SM.name AS schname
	FROM	sys.objects AS RO
		INNER JOIN sys.sql_modules AS SC ON RO.object_id = SC.object_id
		INNER JOIN sys.types AS T ON T.rule_object_id = RO.object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = RO.schema_id
	WHERE	RO.type = 'R'
	UNION
	SELECT
		RO.[name] as [rname],
		PO.[name] as [oname],
		C.[name] as [cname],
		SC.[definition] as [rtext],
		SM.name AS schname
	FROM	sys.objects AS RO
		INNER JOIN sys.sql_modules AS SC ON RO.object_id = SC.object_id
		INNER JOIN sys.columns AS C ON RO.object_id = C.rule_object_id
		INNER JOIN sys.objects AS PO ON C.object_id = PO.object_id
			AND PO.type = 'U'
		INNER JOIN sys.types AS T ON C.user_type_id = T.user_type_id
			AND T.rule_object_id <> RO.object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = RO.schema_id
	WHERE	RO.type = 'R'
	UNION
	SELECT
		RO.[name] as [rname],
		CONVERT(sysname, NULL) as [oname],
		CONVERT(sysname, NULL) as [cname],
		SC.[definition] as [rtext],
		SM.name AS schname
	FROM	sys.objects AS RO
		INNER JOIN sys.sql_modules AS SC ON RO.object_id = SC.object_id
		LEFT OUTER JOIN sys.types AS T ON T.rule_object_id = RO.object_id
		LEFT OUTER JOIN sys.columns AS C
			INNER JOIN sys.objects AS PO ON C.object_id = PO.OBJECT_ID
				AND PO.type = 'U'
		ON RO.object_id = C.rule_object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = RO.schema_id
	WHERE	RO.type = 'R'
		AND C.rule_object_id IS NULL
		AND T.rule_object_id IS NULL	

