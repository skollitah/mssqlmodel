EXEC dbm.db_model_utl_createProc 'db_model_print_output'
GO

ALTER PROCEDURE dbm.db_model_print_output
        @tablename sysname
AS
BEGIN

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF
	
	DECLARE @sql varchar(8000)
	
	CREATE TABLE #output
	(
		[row] numeric(10),
		[batchend] bit,
		[code] nvarchar(MAX),
	)
	SET @sql =
	'
		INSERT INTO #output
		(
			[row],
			[batchend],
			[code]
		)
			SELECT	
				[row],
				[batchend],
				[code]
			FROM	' + @tablename + '
	'
	
	EXEC (@sql)
		
	DECLARE @ibatchend bit
	DECLARE @icode nvarchar(MAX)
	
	DECLARE resultCursor CURSOR FAST_FORWARD FOR
		SELECT	
			[batchend],
			[code]
		FROM	#output
		ORDER BY
			[row]
	
	OPEN resultCursor
	
	FETCH NEXT FROM resultCursor INTO @ibatchend, @icode
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC dbm.db_model_print_long_sql @icode, NULL
		IF @ibatchend = 1
		BEGIN
			PRINT 'GO'
		END
		FETCH NEXT FROM resultCursor INTO @ibatchend, @icode
	END
	CLOSE resultCursor
	DEALLOCATE resultCursor
	
END