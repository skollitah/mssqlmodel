EXEC dbm.db_model_utl_createProc 'db_model_drop_functions'
GO

ALTER PROCEDURE dbm.db_model_drop_functions
(
	@schname sysname,
	@name sysname = NULL
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_functions 'dbo', 'Closest_FX_Rate'
EXEC dbm.db_model_drop_functions 'dbo'
ROLLBACK TRAN modelupdate
SELECT * FROM dbm.db_model_get_functions
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1, 1),
		[schname] sysname,
		[name] sysname,
		[type] varchar(10)
	)

	INSERT INTO #db_objects
	(
		[schname],
		[name],
		[type]
	)
		SELECT
			DISTINCT
			G.[schname],
			G.[fname],
			G.[ftype]
		FROM	db_model_get_functions AS G
		WHERE	(G.fname = @name
			OR @name IS NULL)
			AND G.schname = @schname

	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @iotype sysname
	DECLARE @sql varchar(1000)
	
	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			O.[schname],
			O.[name],
			O.[type]
		FROM	#db_objects AS O
	
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @iotype
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = ('DROP FUNCTION [' + @ischname + '].[' + @ioname + ']')
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @iotype
	END
	
	CLOSE dropObjects
	DEALLOCATE dropObjects
END
