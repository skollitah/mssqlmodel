EXEC dbm.db_model_utl_createProc 'db_model_save_functions'
GO

ALTER PROCEDURE dbm.db_model_save_functions
(
	@schname sysname,
	@name sysname = NULL
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_functions 'dbo', 'Closest_FX_Rate'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_functions 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_functions
DROP TABLE dbm.db_model_saved_functions
SELECT * FROM dbm.db_model_get_functions
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_functions', NULL) = 0 
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_functions
		FROM	dbm.db_model_get_functions AS G
		WHERE	(G.fname = @name
			OR @name IS NULL)
			AND G.schname = @schname
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_functions
			SELECT	*
			FROM	dbm.db_model_get_functions AS G
			WHERE	(G.fname = @name
				OR @name IS NULL)
				AND G.schname = @schname
	END
END
