EXEC dbm.db_model_utl_CreateFuncTF 'db_model_utl_List2Table'
GO
alter function dbm.db_model_utl_List2Table ( @pList varchar(4000),@separator varchar(1))
returns @pvalues Table ( value varchar(4000))
as
begin

declare @i as int ,@j as int ,@k as int
select @i=1

while @i <=len(@pList)
begin

	select @j=patindex('%'+@separator+'%',substring(@pList,@i,len(@pList)-@i+1 ))
	if @j > 0
		begin		
			insert into @pValues  select LTRIM(RTRIM(substring(@pList,@i ,@j-1)))
				select @i=@i+@j
		end
	else
		begin
			insert into @pValues  select LTRIM(RTRIM(substring(@pList,@i,len(@pList)-@i+1)))		
				select @i=len(@pList)+1
		end

end

return 
end
GO