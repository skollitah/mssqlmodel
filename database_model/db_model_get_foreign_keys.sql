EXEC dbm.db_model_utl_createView 'db_model_get_foreign_keys'
GO
	
ALTER VIEW dbm.db_model_get_foreign_keys
AS
-- select * from dbm.db_model_get_foreign_keys
	SELECT	FK1.[name] AS [kname],
		FP.[name] AS [foname],
		CP.[name] AS [fcname],
		FR.[name] AS [roname],
		CR.[name] AS [rcname],
		ROW_NUMBER() OVER (PARTITION BY FK1.[name] ORDER BY FC.constraint_column_id) AS [pos],
		CASE
			WHEN FK1.is_system_named = 1 THEN 'N'
			ELSE 'Y'
		END AS ExplicitDef,
		SM.name AS schname			
	FROM	sys.objects AS FKO
		INNER JOIN sys.foreign_keys AS FK1 ON FKO.object_id = FK1.object_id
		INNER JOIN sys.objects AS FP ON FK1.parent_object_id = FP.object_id
		INNER JOIN sys.objects AS FR ON FK1.referenced_object_id = FR.object_id
		INNER JOIN sys.foreign_key_columns AS FC ON FC.constraint_object_id = FKO.object_id
			AND FC.parent_object_id = FP.object_id
			AND FC.referenced_object_id = FR.object_id
		INNER JOIN sys.columns AS CP ON FC.parent_column_id = CP.column_id
			AND CP.object_id = FP.object_id
		INNER JOIN sys.columns AS CR ON FC.referenced_column_id = CR.column_id 
			AND CR.object_id = FR.object_id	
		INNER JOIN sys.schemas AS SM ON SM.schema_id = FKO.schema_id

