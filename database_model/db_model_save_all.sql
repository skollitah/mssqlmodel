EXEC dbm.db_model_utl_createProc 'db_model_save_all'
GO

ALTER PROCEDURE dbm.db_model_save_all
(
	@schname sysname
)
AS BEGIN
/*
EXEC dbm.db_model_save_all 'dbo'
EXEC dbm.db_model_save_all NULL
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @ioname sysname
	DECLARE @sql varchar(2000)

	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT	DISTINCT
			G.pname
		FROM	dbm.db_model_get_procedures AS G
		WHERE	G.pname LIKE 'db_model_save%'
			AND G.pname <> 'db_model_save_all'
			AND G.schname = 'dbm'
		
	OPEN dropObjects
	FETCH NEXT FROM dropObjects INTO @ioname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql ='EXEC dbm.[' + @ioname + '] ' + ISNULL('''' + @schname + '''', 'NULL')
		--PRINT @sql
		EXEC(@sql)
		FETCH NEXT FROM dropObjects INTO @ioname
	END
	
	CLOSE dropObjects
	DEALLOCATE dropObjects
END
