IF EXISTS (
	SELECT	1
	FROM	sys.objects AS O
		INNER JOIN sys.schemas AS SM ON O.schema_id = SM.schema_id
	WHERE	O.name = 'db_model_utl_TableToList'
		AND SM.name = 'dbm'
		AND o.type = 'FN'
)
BEGIN
	DROP FUNCTION dbm.db_model_utl_TableToList
END
GO
 
CREATE FUNCTION dbm.db_model_utl_TableToList
(
	@table AS TableOfColumns READONLY,
	@separator AS varchar(2)
)
RETURNS varchar(max)
AS BEGIN

	DECLARE @result varchar(max) = ''
	DECLARE @tableTmp AS TABLE
	(
		ColumnName sysname,
		ColumnPos int
	)
	
	INSERT INTO @tableTmp(ColumnName, ColumnPos)
		SELECT	ColumnName,
			ColumnPos
		FROM	@table
		ORDER BY
			ColumnPos

	UPDATE	T
	SET	@result = @result + T.ColumnName + @separator
	FROM	@tableTmp AS T
	
	IF LTRIM(@result) = ''
	BEGIN
		RETURN NULL
	END
	ELSE	
	BEGIN
		SET @result = LEFT(@result, LEN(@result) - 1)
	END
	
	RETURN @result
END