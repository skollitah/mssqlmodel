EXEC dbm.db_model_utl_createView 'db_model_get_functions'
GO

ALTER VIEW dbm.db_model_get_functions
AS
-- select * from dbm.db_model_get_functions
	SELECT
		O.[name] as [fname],
		RTRIM(O.[type]) as [ftype],
		C.[definition] as [ftext],
		SM.name AS schname
	FROM	sys.objects AS O
		INNER JOIN sys.sql_modules AS C ON o.object_id = c.object_id
			AND O.[type] in ('IF', 'TF', 'FN')
		INNER JOIN sys.schemas AS SM ON SM.schema_id = O.schema_id