EXEC dbm.db_model_utl_createProc 'db_model_drop_checks'
GO

ALTER PROCEDURE dbm.db_model_drop_checks
(
	@schname sysname,
	@kname sysname = NULL,
	@oname sysname = NULL,
	@columnList dbm.TableOfColumns READONLY -- not nullable, must be provided, but can be empty
)
AS BEGIN

/*	
BEGIN TRAN modelupdate
DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('ReductionPercent', 1), ('LoadPercent', 2), ('FeeAmount', 3)
EXEC dbm.db_model_drop_checks 'dbo', 'CK_TXLCC_ONE_OF_THREE', 'TransactionLoadComponentCorrection', @t
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
DECLARE @t AS dbm.TableOfColumns
EXEC dbm.db_model_drop_checks 'dbo', 'CK_testcheck', NULL , @t
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('ReductionPercent', 1), ('LoadPercent', 2), ('FeeAmount', 3)
EXEC dbm.db_model_drop_checks 'dbo', NULL, 'TransactionLoadComponentCorrection', @t
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_checks 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_get_checks
*/
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @ikname sysname
	DECLARE @sql varchar(1000)
	DECLARE @obj AS TABLE
	(
		kname sysname,
		oname sysname
	)
	
	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1, 1),
		[schname] sysname,
		[oname] sysname,
		[kname] sysname
	)

	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	BEGIN
		WITH CList AS
		(
			SELECT	U.oname,
				U.kname,
				U.cname,
				U.pos AS Pos
			FROM	dbm.db_model_get_checks AS U
			WHERE	U.oname = @oname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, oname)
			SELECT
				C1.kname,
				C1.oname
			FROM
			(
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @columnList AS T ON T.ColumnName = U.cname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.oname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.oname
				) AS C2	ON C1.kname = C2.kname
					AND C1.oname = C2.oname
					AND C1.CNT = C2.CNT
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @columnList)
	END
	
INSERT INTO #db_objects
	(
		[schname],
		[kname],
		[oname]
	)
		SELECT
			DISTINCT
			G.[schname],
			G.kname,
			G.oname
		FROM	dbm.db_model_get_checks AS G
		WHERE
			(
			G.kname = @kname
				AND G.oname = @oname
				AND EXISTS (
					SELECT	1
					FROM	@obj AS CL
					WHERE	CL.kname = G.kname
						AND CL.oname = G.oname
					)
			OR @kname IS NULL
				AND G.oname = @oname
				AND EXISTS (
					SELECT	1
					FROM	@obj AS CL
					WHERE	CL.kname = G.kname
						AND CL.oname = G.oname
					)
			OR G.kname = @kname
				AND @oname IS NULL
				AND NOT EXISTS (SELECT 1 FROM @columnList)		
			OR @kname IS NULL
				AND @oname IS NULL
				AND NOT EXISTS (SELECT 1 FROM @columnList)
			)
			AND G.schname = @schname

	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			O.[schname],
			O.[kname],
			O.[oname]
		FROM	#db_objects AS O
	
	OPEN dropObjects

	FETCH NEXT FROM dropObjects INTO @ischname, @ikname, @ioname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = ('ALTER TABLE [' + @ischname + '].[' + @ioname + '] DROP CONSTRAINT [' +@ikname + ']')
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ikname, @ioname
	END

	CLOSE dropObjects
	DEALLOCATE dropObjects
END
