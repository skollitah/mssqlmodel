EXEC dbm.db_model_utl_createProc 'db_model_save_views'
GO

ALTER PROCEDURE dbm.db_model_save_views
(
	@schname sysname,
	@name sysname = NULL
	
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_views 'dbo', 'vETLFundShareClass'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_views 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_views
DROP TABLE dbm.db_model_saved_views
SELECT * FROM dbm.db_model_get_views
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_views', NULL) = 0 
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_views
		FROM	dbm.db_model_get_views AS G
		WHERE	(G.vname = @name
			OR @name IS NULL)
			AND G.schname = @schname
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_views
			SELECT	*
			FROM	dbm.db_model_get_views AS G
			WHERE	(G.vname = @name
				OR @name IS NULL)
				AND G.schname = @schname
	END
END
