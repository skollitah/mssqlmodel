EXEC dbm.db_model_utl_createProc 'db_model_save_procedures'
GO

ALTER PROCEDURE dbm.db_model_save_procedures
(
	@schname sysname,
	@name sysname = NULL
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_procedures 'dbo', 'app_pcLoadCheck'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_procedures 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_procedures
DROP TABLE dbm.db_model_saved_procedures
SELECT * FROM dbm.db_model_get_procedures
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_procedures', NULL) = 0
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_procedures
		FROM	dbm.db_model_get_procedures AS G
		WHERE	(G.pname = @name
			OR @name IS NULL)
			AND G.schname = @schname
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_procedures
			SELECT	*
			FROM	dbm.db_model_get_procedures AS G
			WHERE	(G.pname = @name
				OR @name IS NULL)
				AND G.schname = @schname
	END
END
