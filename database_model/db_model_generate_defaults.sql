EXEC dbm.db_model_utl_createProc 'db_model_generate_defaults'
GO

ALTER PROCEDURE dbm.db_model_generate_defaults
(
	@schname sysname,
	@source bit, -- 0 - database, 1 - saved objects
	@output smallint, -- 0 - table, 1 - console, 2 - execute
	@bind smallint, -- 0 - no, 1 - yes, 2 - only bind
	@leavExp bit = 0, -- 0 - rename all, 1 - leave explicitly defined names of keys  
	@kname sysname = NULL,
	@oname sysname = NULL,
	@cname sysname = NULL
)
AS BEGIN

/*
EXEC dbm.db_model_save_defaults
DROP TABLE dbm.db_model_saved_defaults

EXEC dbm.db_model_generate_defaults 'dbo', 1, 0, 0, 0
EXEC dbm.db_model_generate_defaults 'dbo', 0, 1, 1, 1
EXEC dbm.db_model_generate_defaults 'dbo', 1, 0, 1, 1
EXEC dbm.db_model_generate_defaults 'dbo', 0, 1, 1, 0, 'D_TestDefault', NULL, NULL
EXEC dbm.db_model_generate_defaults 'dbo', 0, 1, 1, 1, NULL, 'ETLCommit', 'fileoffset'

SELECT * FROM dbm.db_model_get_defaults
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF OBJECT_ID('tempdb..#result') IS NOT NULL
	BEGIN
		DROP TABLE #result
	END
	
	CREATE TABLE #result
	(
		[row] numeric(10) identity(1,1),
		[batchend] bit,
		[code] varchar(MAX),
	)

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1,1),
		[schname] sysname,
		[oname] sysname NULL,
		[kname] sysname NULL,
		[cname] sysname NULL,
		[ktext] varchar(MAX) NULL,
		[explicite] varchar(2) NULL,
		[Independent] varchar(2) NULL
	)

	IF @source = 0
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[oname],
			[kname],
			[cname],
			[ktext],
			[explicite],
			[Independent]
		)
		SELECT
			G.[schname],
			G.[oname],
			G.[dname],
			G.[cname],
			G.[dtext],
			G.[ExplicitDef],
			G.[Independent]
		FROM	dbm.db_model_get_defaults AS G
		WHERE
			(
			G.oname = @oname
				AND G.cname = @cname
				AND G.dname = @kname
			OR G.oname = @oname
				AND G.cname = @cname
				AND @kname IS NULL
			OR G.oname = @oname
				AND @cname IS NULL
				AND @kname IS NULL			
			OR @oname IS NULL
				AND @cname IS NULL
				AND G.dname = @kname
			OR @oname IS NULL
				AND @cname IS NULL
				AND @kname IS NULL
			)
			AND G.schname = @schname
	END
	ELSE IF @source = 1 AND dbm.db_model_exists_table_column('dbm', 'db_model_saved_defaults', NULL) = 1
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[oname],
			[kname],
			[cname],
			[ktext],
			[explicite],
			[Independent]
		)
		SELECT
			G.[schname],
			G.[oname],
			G.[dname],
			G.[cname],
			G.[dtext],
			G.[ExplicitDef],
			G.[Independent]
		FROM	dbm.db_model_saved_defaults AS G
		WHERE
			(
			G.oname = @oname
				AND G.cname = @cname
				AND G.dname = @kname
			OR G.oname = @oname
				AND G.cname = @cname
				AND @kname IS NULL
			OR G.oname = @oname
				AND @cname IS NULL
				AND @kname IS NULL			
			OR @oname IS NULL
				AND @cname IS NULL
				AND G.dname = @kname
			OR @oname IS NULL
				AND @cname IS NULL
				AND @kname IS NULL
			)
			AND G.schname = @schname
	
	END
	
	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @ikname sysname
	DECLARE @icname sysname
	DECLARE @iktext varchar(MAX)
	DECLARE @iexplicite varchar(2)
	DECLARE @indName varchar(2000) = ''
	DECLARE @tmpResult varchar(MAX) = ''
	DECLARE @cmd varchar(2000) = ''
	DECLARE @nl varchar(2) = CHAR(13) + CHAR(10)


	DECLARE generateObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[kname],
			O.[ktext]			
		FROM	#db_objects AS O
		WHERE	O.Independent = 'Y'
		ORDER BY
			O.[kname]
	
	OPEN generateObjects
	
	FETCH NEXT FROM generateObjects INTO @ischname, @ikname, @iktext
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @bind IN (0, 1)
		BEGIN
			set @cmd = replace(@iktext, '''', '''''')
			SET @tmpResult = ''
			SET @tmpResult = @tmpResult + 'IF dbm.db_model_exists_default(''' + @ischname + ''', ''' + @ikname + ''', NULL, NULL) = 0' + @nl
			SET @tmpResult = @tmpResult + 'BEGIN' + @nl
			SET @tmpResult = @tmpResult + '	EXEC ( ''' + @cmd + ''')' + @nl
			SET @tmpResult = @tmpResult + 'END' + @nl
			INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
		END
		
		IF @bind IN (1, 2)
		BEGIN
			DECLARE bindObjects CURSOR FAST_FORWARD FOR
			SELECT
				DISTINCT
				O.[oname],
				O.[cname]			
			FROM	#db_objects AS O
			WHERE	O.kname =  @ikname
				AND O.[oname] IS NOT NULL
				AND O.Independent = 'Y'
				AND O.schname = @ischname
			ORDER BY
				O.[oname]
			
			OPEN bindObjects
			
			FETCH NEXT FROM bindObjects INTO @ioname, @icname
			WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @tmpResult = ''
				SET @tmpResult = @tmpResult + 'IF dbm.db_model_isbinded_default(''' + @ischname + ''', ''' + @ikname + ''', ''' + @ioname + ''', ' + ISNULL('''' + @icname + '''', 'NULL') + ') = 0' + @nl
				SET @tmpResult = @tmpResult + 'BEGIN' + @nl
				IF @icname IS NULL
				BEGIN
					SET @tmpResult = @tmpResult + '	EXEC sys.sp_bindefault ''[' + @ischname + '].[' + @ikname + ']'', ''[' + @ischname + '].[' + @ioname + ']''' + @nl
				END
				ELSE
				BEGIN
					SET @tmpResult = @tmpResult + '	EXEC sys.sp_bindefault ''[' + @ischname + '].[' + @ikname + ']'', ''[' + @ischname + '].[' + @ioname + '].[' + @icname + ']''' + @nl
				END
				SET @tmpResult = @tmpResult + 'END' + @nl
				INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
				FETCH NEXT FROM bindObjects INTO @ioname, @icname
			END
			CLOSE bindObjects
			DEALLOCATE bindObjects
		END
		
		FETCH NEXT FROM generateObjects INTO @ischname, @ikname, @iktext
	END

	CLOSE generateObjects
	DEALLOCATE generateObjects	

	DECLARE generateObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[kname],
			O.[oname],
			O.[cname],
			O.[ktext],
			O.explicite			
		FROM	#db_objects AS O
		WHERE	O.Independent = 'N'
		ORDER BY
			O.[kname]
	
	OPEN generateObjects
	
	FETCH NEXT FROM generateObjects INTO @ischname, @ikname, @ioname, @icname, @iktext, @iexplicite
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @bind = 1
		BEGIN
		
			SET @indName = LEFT(@ioname + '_' + @icname + '_', 126) + 'DF'
			IF @leavExp = 1
			BEGIN
				IF @iexplicite = 'Y'
				BEGIN		
					SET @indName = @ikname
				END
			END
			SET @tmpResult = ''
			SET @tmpResult = @tmpResult + 'IF dbm.db_model_exists_default(''' + @ischname + ''', NULL, ''' + @ioname + ''', ''' + @icname + ''') = 0' + @nl
			SET @tmpResult = @tmpResult + 'BEGIN' + @nl
			SET @tmpResult = @tmpResult + '	ALTER TABLE [' + @ischname + '].[' + @ioname + '] ADD CONSTRAINT [' + @indName + '] DEFAULT ' + @iktext + ' FOR [' +  @icname + ']' + @nl
			SET @tmpResult = @tmpResult + 'END' + @nl
			INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
		END		
		FETCH NEXT FROM generateObjects INTO @ischname, @ikname, @ioname, @icname, @iktext, @iexplicite
	END
		
	CLOSE generateObjects
	DEALLOCATE generateObjects	
	
	
	IF @output = 0
	BEGIN
		SELECT	*
		FROM	#result
	END
	ELSE IF @output = 1
	BEGIN
		EXEC dbm.db_model_print_output '#result'
	END
	ELSE IF @output = 2
	BEGIN
		EXEC dbm.db_model_execute_output '#result'
	END
END