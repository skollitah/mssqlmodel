EXEC dbm.db_model_utl_createProc 'db_model_drop_foreign_keys'
GO

ALTER PROCEDURE dbm.db_model_drop_foreign_keys
(
	@schname sysname,
	@kname sysname = NULL,
	@foname sysname = NULL,
	@fColumnList dbm.TableOfColumns READONLY, -- not nullable, must be provided, but can be empty
	@roname sysname = NULL,
	@rColumnList dbm.TableOfColumns READONLY -- not nullable, must be provided, but can be empty	
)
AS BEGIN

/*	
BEGIN TRAN modelupdate
DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('ShareInShareClass_Fund_id', 1), ('ShareInShareClass_ShareClass_id', 2)
EXEC dbm.db_model_drop_foreign_keys 'dbo', 'FK_Blockade_ShareInShareClass', 'Blockade', @t
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
DECLARE @tf AS dbm.TableOfColumns
DECLARE @tr AS dbm.TableOfColumns
EXEC dbm.db_model_drop_foreign_keys 'dbo', 'FK_Blockade_ShareInShareClass', NULL, @tf, NULL,  @tr
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
DECLARE @tf AS dbm.TableOfColumns
DECLARE @tr AS dbm.TableOfColumns
INSERT INTO @tf (ColumnName, ColumnPos) VALUES ('ShareInShareClass_Fund_id', 1), ('ShareInShareClass_ShareClass_id', 2)
INSERT INTO @tr (ColumnName, ColumnPos) VALUES ('Fund_id', 1), ('ShareClass_id', 2)
EXEC dbm.db_model_drop_foreign_keys 'dbo', NULL, 'Blockade', @tf, 'ShareInShareClass', @tr
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_foreign_keys 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_get_foreign_keys
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @ikname sysname
	DECLARE @sql varchar(1000)
	DECLARE @obj AS TABLE
	(
		kname sysname,
		foname sysname,
		roname sysname
	)
	
	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1, 1),
		[schname] sysname,
		[oname] sysname,
		[kname] sysname
	)
	
	IF @foname IS NOT NULL AND @roname is NOT NULL
		AND EXISTS (SELECT 1 FROM @fColumnList) AND EXISTS (SELECT 1 FROM @rColumnList)
	BEGIN
		WITH CList AS
		(
			SELECT
				U.foname,
				U.fcname,
				U.roname,
				U.rcname,
				U.kname,
				U.pos AS Pos
			FROM	dbm.db_model_get_foreign_keys AS U
			WHERE	U.foname = @foname
				AND U.roname = @roname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, foname, roname)
			SELECT
				C1.kname,
				C1.foname,
				C1.roname
			FROM
			(
				SELECT	
					U.kname,
					U.foname,
					U.roname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @fColumnList AS T ON T.ColumnName = U.fcname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.foname,
						U.roname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.foname,
					U.roname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @rColumnList AS T ON T.ColumnName = U.rcname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.foname,
						U.roname
				) AS C2 ON C1.kname = C2.kname
					AND C1.foname = C2.foname
					AND C1.roname = C2.roname
					AND C1.CNT = C2.CNT		
				INNER JOIN (
				SELECT	
					U.kname,
					U.foname,
					U.roname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.foname,
						U.roname
				) AS C3	ON C1.kname = C3.kname
					AND C1.foname = C3.foname
					AND C1.roname = C3.roname
					AND C1.CNT = C3.CNT		
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @fColumnList)
				AND C1.CNT = (SELECT COUNT(*) FROM @rColumnList)
	END
	

	INSERT INTO #db_objects
	(
		[schname],
		[kname],
		[oname]
	)
		SELECT
			DISTINCT
			G.[schname],
			G.kname,
			G.foname
		FROM	dbm.db_model_get_foreign_keys AS G
		WHERE
			(
			G.kname = @kname
				AND G.foname = @foname
				AND G.roname = @roname
				AND EXISTS (
					SELECT	1
					FROM	@obj AS CL
					WHERE	CL.kname = G.kname
						AND CL.foname = G.foname
						AND CL.roname = G.roname
					)
			OR @kname IS NULL
				AND G.foname = @foname
				AND G.roname = @roname
				AND EXISTS (
					SELECT	1
					FROM	@obj AS CL
					WHERE	CL.kname = G.kname
						AND CL.foname = G.foname
						AND CL.roname = G.roname
					)
			OR G.kname = @kname
				AND @foname IS NULL
				AND @roname IS NULL
				AND NOT EXISTS (SELECT 1 FROM @fColumnList)
				AND NOT EXISTS (SELECT 1 FROM @rColumnList)	
			OR @kname IS NULL
				AND @foname IS NULL
				AND @roname IS NULL
				AND NOT EXISTS (SELECT 1 FROM @fColumnList)
				AND NOT EXISTS (SELECT 1 FROM @rColumnList)	
			)
			AND G.schname = @schname	

	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			O.[schname],
			O.[oname],
			O.[kname]
		FROM	#db_objects AS O
	
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @ikname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = ('ALTER TABLE [' + @ischname + '].[' + @ioname + '] DROP CONSTRAINT [' +@ikname + ']')
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @ikname
	END

	CLOSE dropObjects
	DEALLOCATE dropObjects	
END
