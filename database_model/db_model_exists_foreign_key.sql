EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_foreign_key'
GO

ALTER FUNCTION dbm.db_model_exists_foreign_key
(
	@schname sysname,
	@kname sysname,
	@foname sysname,
	@fColumnList dbm.TableOfColumns READONLY, -- not nullable, must be provided, but can be empty
	@roname sysname,
	@rColumnList dbm.TableOfColumns READONLY -- not nullable, must be provided, but can be empty
) RETURNS smallint
AS BEGIN

/*
SELECT * FROM dbm.db_model_get_foreign_keys

DECLARE @tf AS dbm.TableOfColumns
DECLARE @tr AS dbm.TableOfColumns
INSERT INTO @tf (ColumnName, ColumnPos) VALUES ('ShareInShareClass_Fund_id', 1), ('ShareInShareClass_ShareClass_id', 2)
INSERT INTO @tr (ColumnName, ColumnPos) VALUES ('Fund_id', 1), ('ShareClass_id', 2)
SELECT dbm.db_model_exists_foreign_key('dbo', 'FK_Blockade_ShareInShareClass', 'Blockade', @tf, 'ShareInShareClass', @tr)

DECLARE @tf AS dbm.TableOfColumns
DECLARE @tr AS dbm.TableOfColumns
INSERT INTO @tf (ColumnName, ColumnPos) VALUES ('ShareInShareClass_Fund_id', 1), ('ShareInShareClass_ShareClass_id', 2)
INSERT INTO @tr (ColumnName, ColumnPos) VALUES ('Fund_id', 1), ('ShareClass_id', 2)
SELECT dbm.db_model_exists_foreign_key('dbo', NULL, 'Blockade', @tf, 'ShareInShareClass', @tr)

DECLARE @tf AS dbm.TableOfColumns
DECLARE @tr AS dbm.TableOfColumns
SELECT dbm.db_model_exists_foreign_key('dbo', 'FK_AcceptanceGroup_Provider', NULL, @tf, NULL, @tr)

SELECT dbm.db_model_exists_foreign_key('dbo', 'FK_AcceptanceGroup_Provider', NULL, DEFAULT, NULL, DEFAULT)

SELECT dbm.db_model_exists_foreign_key(NULL, NULL, NULL, DEFAULT, NULL, DEFAULT)
*/
	
	DECLARE @result smallint
	DECLARE @obj AS TABLE
	(
		kname sysname,
		foname sysname,
		roname sysname
	)		
	
	IF
	(	
		(COALESCE(@foname, @roname) IS NOT NULL OR EXISTS (SELECT 1 FROM @fColumnList) OR EXISTS (SELECT 1 FROM @rColumnList))
		AND (@foname IS NULL OR @roname IS NULL OR NOT EXISTS (SELECT 1 FROM @fColumnList) OR NOT EXISTS (SELECT 1 FROM @rColumnList))
	)
		OR
	(	
		@schname IS NULL	
	)
	BEGIN
		SET @result = -1
		RETURN @result
	END
	
	
	IF @foname IS NOT NULL AND @roname is NOT NULL
		AND EXISTS (SELECT 1 FROM @fColumnList) AND EXISTS (SELECT 1 FROM @rColumnList)
	BEGIN
		WITH CList AS
		(
			SELECT
				U.foname,
				U.fcname,
				U.roname,
				U.rcname,
				U.kname,
				U.pos AS Pos
			FROM	dbm.db_model_get_foreign_keys AS U
			WHERE	U.foname = @foname
				AND U.roname = @roname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, foname, roname)
			SELECT
				C1.kname,
				C1.foname,
				C1.roname
			FROM
			(
				SELECT	
					U.kname,
					U.foname,
					U.roname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @fColumnList AS T ON T.ColumnName = U.fcname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.foname,
						U.roname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.foname,
					U.roname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @rColumnList AS T ON T.ColumnName = U.rcname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.foname,
						U.roname
				) AS C2 ON C1.kname = C2.kname
					AND C1.foname = C2.foname
					AND C1.roname = C2.roname
					AND C1.CNT = C2.CNT		
				INNER JOIN (
				SELECT	
					U.kname,
					U.foname,
					U.roname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.foname,
						U.roname
				) AS C3	ON C1.kname = C3.kname
					AND C1.foname = C3.foname
					AND C1.roname = C3.roname
					AND C1.CNT = C3.CNT		
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @fColumnList)
				AND C1.CNT = (SELECT COUNT(*) FROM @rColumnList)
	END


	DECLARE @cname sysname = 
	(
		SELECT	TOP 1
			G.[kname]
		FROM	dbm.db_model_get_foreign_keys AS G
		WHERE	G.[kname] = @kname
			AND G.schname = @schname
	)
	
	IF @foname IS NULL AND @roname IS NULL
		AND NOT EXISTS (SELECT 1 FROM @fColumnList)
		AND NOT EXISTS (SELECT 1 FROM @rColumnList)
		AND @kname IS NOT NULL
	BEGIN
		IF @cname = @kname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	
	IF @foname IS NOT NULL AND @roname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @fColumnList)
		AND EXISTS (SELECT 1 FROM @rColumnList)
		AND @kname IS NULL
	BEGIN
		IF EXISTS
		(
			SELECT	1
			FROM	@obj
		)
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	
	IF @foname IS NOT NULL AND @roname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @fColumnList)
		AND EXISTS (SELECT 1 FROM @rColumnList)
		AND @kname IS NOT NULL
	BEGIN
		IF EXISTS
		(
			SELECT	1
			FROM	@obj
		)
		AND @kname = @cname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	
	RETURN @result
END
