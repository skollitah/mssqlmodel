DECLARE dropDbmObjects CURSOR FAST_FORWARD FOR
	SELECT
		CASE O.Type
			WHEN 'FN' THEN 
				'IF OBJECT_ID(''' + SM.name + '.' + O.NAME + ''', ''FN'') IS NOT NULL BEGIN DROP FUNCTION ' + SM.name + '.' + O.NAME + ' END'		
			WHEN 'TF' THEN
				'IF OBJECT_ID(''' + SM.name + '.' + O.NAME + ''', ''TF'') IS NOT NULL BEGIN DROP FUNCTION ' + SM.name + '.' + O.NAME + ' END'
			WHEN 'IF' THEN
				'IF OBJECT_ID(''' + SM.name + '.' + O.NAME + ''', ''IF'') IS NOT NULL BEGIN DROP FUNCTION ' + SM.name + '.' + O.NAME + ' END'		
			WHEN 'P' THEN
				'IF OBJECT_ID(''' + SM.name + '.' + O.NAME + ''', ''P'') IS NOT NULL BEGIN DROP PROCEDURE ' + SM.name + '.' + O.NAME + ' END'		
			END AS sqlCode
		--	WHEN 'V' THEN
		--		'IF OBJECT_ID(''' + SM.name + '.' + O.NAME + ''', ''V'') IS NOT NULL BEGIN DROP VIEW ' + SM.name + '.' + O.NAME + ' END'		
		--	WHEN 'U' THEN
		--		'IF OBJECT_ID(''' + SM.name + '.' + O.NAME + ''', ''U'') IS NOT NULL BEGIN DROP TABLE ' + SM.name + '.' + O.NAME + ' END'		
		--END AS sqlCode
	FROM	sys.objects AS O
		INNER JOIN sys.schemas AS SM ON O.schema_id = SM.schema_id
	WHERE	SM.name = 'dbo'
		AND o.type in ('FN', 'TF', 'IF', 'P')
	ORDER BY
		O.type,
		O.name
		
DECLARE @sSql varchar(2000)

OPEN dropDbmObjects

WHILE 1 = 1
BEGIN
	FETCH NEXT FROM dropDbmObjects INTO @sSql
	
	IF @@FETCH_STATUS <> 0
	BEGIN
		BREAK
	END
	
	PRINT @sSql
	--EXEC(@sSql)
	
END 

CLOSE dropDbmObjects
DEALLOCATE dropDbmObjects

