EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_primary_key'
GO

ALTER FUNCTION dbm.db_model_exists_primary_key
(
	@schname sysname,
	@kname sysname,
	@oname sysname,
	@columnList dbm.TableOfColumns READONLY -- not nullable, must be provided, but can be empty
) RETURNS smallint
AS BEGIN

/*
SELECT * FROM dbm.db_model_get_primary_keys

DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('Fund_id', 1), ('ShareClass_id', 2), ('FundShareClassExternalCode_id', 3)
SELECT dbm.db_model_exists_primary_key(NULL, 'FundShareClassExternalCodeValue', @t, 'dbo')

DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('AccountPayment_id', 1)
SELECT dbm.db_model_exists_primary_key('PK_AccountPayment', 'AccountPayment', @t, 'dbo')

DECLARE @t AS dbm.TableOfColumns
SELECT dbm.db_model_exists_primary_key('PK_FUNDSHARECLASSEXTERNALCODEVALUE', NULL, @t, 'dbo')

SELECT dbm.db_model_exists_primary_key('PK_FUNDSHARECLASSEXTERNALCODEVALUE', NULL, DEFAULT, 'dbo')

SELECT dbm.db_model_exists_primary_key(NULL, NULL, DEFAULT, NULL)
*/
	DECLARE @result smallint
	DECLARE @cname sysname
	DECLARE @obj AS TABLE
	(
		kname sysname,
		oname sysname
	)	
	
	IF
	(
		@oname IS NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	)
		OR
	(
		@oname IS NOT NULL
		AND NOT EXISTS (SELECT 1 FROM @columnList)
	)
		OR
	(
		@oname IS NULL
		AND NOT EXISTS (SELECT 1 FROM @columnList)
		AND @kname IS NULL
	)
		OR
	(	
		@schname IS NULL	
	)			
	BEGIN
		SET @result = -1
		RETURN @result
	END
	
	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	BEGIN
		WITH CList AS
		(
			SELECT	U.oname,
				U.kname,
				U.cname,
				U.pos AS Pos
			FROM	dbm.db_model_get_primary_keys AS U
			WHERE	U.oname = @oname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, oname)
			SELECT
				C1.kname,
				C1.oname
			FROM
			(
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @columnList AS T ON T.ColumnName = U.cname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.oname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.oname
				) AS C2	ON C1.kname = C2.kname
					AND C1.oname = C2.oname
					AND C1.CNT = C2.CNT
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @columnList)
	END

	SET @cname = 
	(
		SELECT	TOP 1
			G.[kname]
		FROM	dbm.db_model_get_primary_keys AS G
		WHERE	G.[kname] = @kname
			AND G.schname = @schname
	)
		
	IF @oname IS NULL
		AND NOT EXISTS (SELECT 1 FROM @columnList)
		AND @kname IS NOT NULL
	BEGIN
		IF @cname = @kname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	
	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
		AND @kname IS NULL
	BEGIN
		IF EXISTS
		(
			SELECT	1
			FROM	@obj
		)
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	

	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
		AND @kname IS NOT NULL
	BEGIN
		IF EXISTS
		(
			SELECT	1
			FROM	@obj
		)
		AND @kname = @cname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END	

	RETURN @result
end
