EXEC dbm.db_model_utl_createProc 'db_model_drop_defaults'
GO

ALTER PROCEDURE dbm.db_model_drop_defaults
(
	@schname sysname,
	@kname sysname = NULL,
	@oname sysname = NULL,
	@cname sysname = NULL
)
AS BEGIN

/*	
BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_defaults 'dbo', NULL, 'ETLCommit', 'fileoffset'
--EXEC dbm.db_model_drop_defaults 'dbo', NULL, 'ETLCommit'
--EXEC dbm.db_model_drop_defaults 'dbo', 'DF__ETLCommit__fileo__5020B295', 'ETLCommit', 'fileoffset'
--EXEC dbm.db_model_drop_defaults 'dbo', 'DF__ShareInSh__Equal__27CED166', 'ShareInShareClass'
--EXEC dbm.db_model_drop_defaults 'dbo', 'DF__ShareInSh__Equal__27CED166', NULL, NULL
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_defaults 'dbo'
ROLLBACK TRAN modelupdate
SELECT * FROM dbm.db_model_get_defaults
*/
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF
	
	IF
	(	
		@oname IS NULL
		AND @cname IS NOT NULL
	)
	BEGIN
		RAISERROR('Invalid  parameters', 16, 1)
		RETURN
	END

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1, 1),
		[schname] sysname NULL,
		[oname] sysname NULL,
		[cname] sysname NULL,
		[kname] sysname NULL,
		[explicite] varchar(2) NULL,
		[Independent] varchar(2) NULL
	)
	
	INSERT INTO #db_objects
	(
		[schname],
		[oname],
		[cname],
		[kname],
		[explicite],
		[Independent]
	)
		SELECT
			DISTINCT
			G.[schname],
			G.oname,
			G.cname,
			G.dname,
			G.ExplicitDef,
			G.[Independent]
		FROM	dbm.db_model_get_defaults AS G
		WHERE
			(
			G.oname = @oname
				AND G.cname = @cname
				AND G.dname = @kname
			OR G.oname = @oname
				AND G.cname = @cname
				AND @kname IS NULL
			OR G.oname = @oname
				AND @cname IS NULL
				AND @kname IS NULL			
			OR @oname IS NULL
				AND @cname IS NULL
				AND G.dname = @kname
			OR @oname IS NULL
				AND @cname IS NULL
				AND @kname IS NULL
			)
			AND G.[schname] = @schname

	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @icname sysname
	DECLARE @ikname sysname
	DECLARE @explicite varchar(2)
	DECLARE @sql varchar(1000)

	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[oname],
			O.[cname]
		FROM	#db_objects AS O
		WHERE	O.Independent = 'Y'
			AND O.oname IS NOT NULL
	
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @icname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @icname IS NOT NULL
		BEGIN
			SET @sql = ('EXEC sys.sp_unbindefault ''[' + @ischname + '].[' + @ioname + '].[' + @icname + ']''')
		END
		ELSE
		BEGIN
			SET @sql = ('EXEC sys.sp_unbindefault ''[' + @ischname + '].[' + @ioname + ']''')
		END
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @icname
	END

	CLOSE dropObjects
	DEALLOCATE dropObjects	

	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[kname],
			O.explicite
		FROM	#db_objects AS O
		WHERE	O.Independent = 'Y'
		
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ikname, @explicite
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = ('DROP DEFAULT [' + @ischname + '].[' + @ikname + ']')
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ikname, @explicite
	END

	CLOSE dropObjects
	DEALLOCATE dropObjects


	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[kname],
			O.[oname],
			O.explicite
		FROM	#db_objects AS O
		WHERE	O.Independent = 'N'
	
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ikname, @ioname, @explicite
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = ('ALTER TABLE [' + @ischname + '].[' + @ioname + '] DROP CONSTRAINT [' + @ikname + ']')
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ikname, @ioname, @explicite
	END

	CLOSE dropObjects
	DEALLOCATE dropObjects
END
