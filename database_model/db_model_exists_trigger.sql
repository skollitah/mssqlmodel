EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_trigger'
GO

ALTER FUNCTION dbm.db_model_exists_trigger
(
	@schname sysname,
	@name sysname
) RETURNS smallint
AS BEGIN

/*
SELECT dbm.db_model_exists_trigger('tr_AccountDistributorOrderContext', 'dbo')
SELECT dbm.db_model_exists_trigger(NULL, NULL)
SELECT * FROM dbm.db_model_get_triggers
*/

	DECLARE @result smallint
	
	IF @name IS NULL
		OR @schname IS NULL
	BEGIN
		SET @result = -1
		RETURN @result
	END
	
	IF EXISTS
	(
		SELECT	1 
		FROM	dbm.db_model_get_triggers AS G
		WHERE	G.[tname] = @name
			AND G.schname = @schname
	)
	BEGIN
		SET @result = 1
	END
	ELSE
	BEGIN
		SET @result = 0
	END
		
	RETURN @result
END
