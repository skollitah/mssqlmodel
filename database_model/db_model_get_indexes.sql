EXEC dbm.db_model_utl_createView 'db_model_get_indexes'
GO

ALTER VIEW dbm.db_model_get_indexes
AS
-- select * from dbm.db_model_get_indexes order by [oname], [iname], [keyno]
	SELECT	PO.[name] as [oname],
		SI.[name] as [iname],
		C.[name] as [cname],
		SIC.key_ordinal as [keyno],
		CASE
			WHEN SIC.is_included_column = 1 THEN 'Y'
			ELSE 'N'
		END AS [included],		
		CASE
			WHEN SIC.is_descending_key = 1 THEN 'Y'
			ELSE 'N'
		END AS [desc],	
		CASE
			WHEN SI.is_unique = 1 THEN 'Y'
			ELSE 'N'
		END AS [unique],
		CASE
			WHEN SI.type = 0 THEN 'H'
			WHEN SI.type = 1 THEN 'Y'
			WHEN SI.type = 2 THEN 'N'
			WHEN SI.type = 3 THEN 'X'
		END AS [clustered],
		SM.name AS schname
	FROM	sys.objects AS PO
		INNER JOIN sys.indexes AS SI ON PO.object_id = SI.object_id
			AND PO.type = 'U'
			AND SI.is_hypothetical = 0
			AND SI.is_primary_key = 0
			AND SI.is_unique_constraint = 0
		INNER JOIN sys.index_columns AS SIC ON SI.index_id = SIC.index_id
			AND SI.object_id = SIC.object_id
		INNER JOIN sys.columns AS C ON SIC.column_id = C.column_id
			AND SIC.object_id = C.object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = PO.schema_id

