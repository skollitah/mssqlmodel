EXEC dbm.db_model_utl_createFuncFN 'db_model_isbinded_rule'
GO

ALTER FUNCTION dbm.db_model_isbinded_rule
(
	@schname sysname,
	@kname sysname,
	@oname sysname,
	@cname sysname
	
) RETURNS smallint
AS
BEGIN

/*
SELECT dbm.db_model_isbinded_rule('dbo', NULL, 'Customer', 'Customer_idn')
SELECT dbm.db_model_isbinded_rule('dbo', NULL, 'Customer', NULL)
SELECT dbm.db_model_isbinded_rule('dbo', NULL, 'AllocationKind', NULL)
SELECT dbm.db_model_isbinded_rule('dbo', 'rlj', 'Customer', 'Customer_idn')
SELECT dbm.db_model_isbinded_rule('dbo', 'xx', NULL, NULL)
SELECT dbm.db_model_isbinded_rule('dbo', 'rlj', NULL, 'Customer_idn')
SELECT dbm.db_model_isbinded_rule(NULL, NULL, NULL, NULL)
SELECT * FROM db_model_get_rules
*/

	DECLARE @result smallint
	
	IF @kname IS NULL
			AND @oname IS NULL
		OR @kname IS NOT NULL
			AND @oname IS NULL
			AND @cname IS NOT NULL
		OR @schname IS NULL
	BEGIN
		SET @result = -1
	END
	ELSE
	BEGIN
		IF EXISTS
		(
			SELECT	1 
			FROM	dbm.db_model_get_rules AS G
			WHERE
				(
				G.[rname] = @kname
					AND G.[oname] = @oname
					AND G.[cname] = @cname
				OR G.[rname] = @kname
					AND G.[oname] = @oname
					AND @cname IS NULL AND G.[cname] IS NULL
				OR G.[rname] = @kname
					AND @oname IS NULL AND G.oname IS NOT NULL
					AND @cname IS NULL
				OR @kname IS NULL
					AND G.[oname] = @oname
					AND G.[cname] = @cname
				OR @kname IS NULL
					AND G.[oname] = @oname
					AND @cname IS NULL AND G.[cname] IS NULL
				)
				AND G.schname =  @schname
		)
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	
	RETURN @result
END
