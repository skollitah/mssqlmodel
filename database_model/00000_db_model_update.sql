IF NOT EXISTS
(
	SELECT	1
	FROM	sys.schemas AS SM
	WHERE	SM.name = 'dbm'
)
BEGIN
	EXEC ('CREATE SCHEMA dbm')
END


IF NOT EXISTS (
	SELECT	1
	FROM	sys.table_types AS TT
		INNER JOIN sys.schemas AS SM ON TT.schema_id = SM.schema_id
	WHERE	TT.name =  'TableOfColumns'
		AND SM.name = 'dbm'
)
BEGIN
	CREATE TYPE dbm.TableOfColumns AS TABLE
	(
		ColumnName sysname,
		ColumnPos int
	)
END
GO