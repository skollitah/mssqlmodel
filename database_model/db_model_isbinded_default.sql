EXEC dbm.db_model_utl_createFuncFN 'db_model_isbinded_default'
GO

ALTER FUNCTION dbm.db_model_isbinded_default
(
	@schname sysname,
	@kname sysname,
	@oname sysname,
	@cname sysname
	
) RETURNS smallint
AS
BEGIN

/*
SELECT dbm.db_model_isbinded_default('dbo', 'DF__Payment__CashFlo__01E2F6B2', 'Payment', 'CashFlowDictionary_id')
SELECT dbm.db_model_isbinded_default('dbo', NULL, 'Provider', NULL)
SELECT dbm.db_model_isbinded_default('dbo', NULL, 'ETLCommit', 'fileoffset')
SELECT dbm.db_model_isbinded_default('dbo', 'dlj', NULL, NULL)
SELECT dbm.db_model_isbinded_default('dbo', 'dlj', NULL, 'Customer_idn')
SELECT dbm.db_model_isbinded_default(NULL, NULL, NULL, NULL)
SELECT * FROM db_model_get_defaults
*/

	DECLARE @result smallint
	
	IF @kname IS NULL
			AND @oname IS NULL
		OR @kname IS NOT NULL
			AND @oname IS NULL
			AND @cname IS NOT NULL
		OR @schname IS NULL
	BEGIN
		SET @result = -1
	END
	ELSE
	BEGIN
		IF EXISTS
		(
			SELECT	1 
			FROM	dbm.db_model_get_defaults AS G
			WHERE
				(
				G.[dname] = @kname
					AND G.[oname] = @oname
					AND G.[cname] = @cname
				OR G.[dname] = @kname
					AND G.[oname] = @oname
					AND @cname IS NULL AND G.[cname] IS NULL
				OR G.[dname] = @kname
					AND @oname IS NULL AND G.oname IS NOT NULL
					AND @cname IS NULL
				OR @kname IS NULL
					AND G.[oname] = @oname
					AND G.[cname] = @cname
				OR @kname IS NULL
					AND G.[oname] = @oname
					AND @cname IS NULL AND G.[cname] IS NULL 
				)
				AND G.schname = @schname
		)
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	
	RETURN @result
END