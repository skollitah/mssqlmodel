EXEC dbm.db_model_utl_createProc 'db_model_generate_triggers'
GO

ALTER PROCEDURE dbm.db_model_generate_triggers
(
	@schname sysname,
	@source bit, -- 0 - database, 1 - saved objects
	@output smallint, -- 0 - table, 1 - console, 2 - execute
	@name sysname = NULL
)
AS BEGIN

/*
EXEC dbm.db_model_save_triggers
DROP TABLE dbm.db_model_saved_triggers

EXEC dbm.db_model_generate_triggers 'dbo', 0, 1, NULL
EXEC dbm.db_model_generate_triggers 'dbo', 0, 2, 'tr_SipProductVerOrderContext'

SELECT * FROM dbm.db_model_get_triggers
*/
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF OBJECT_ID('tempdb..#result') IS NOT NULL
	BEGIN
		DROP TABLE #result
	END
	
	CREATE TABLE #result
	(
		[row] numeric(10) identity(1,1),
		[batchend] bit,
		[code] varchar(MAX)
	)

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1,1),
		[schname] sysname,
		[name] sysname NULL,
		[text] varchar(MAX) NULL
	)

	IF @source = 0
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[name],
			[text]
		)
			SELECT	DISTINCT
				[schname],
				[tname],
				[ttext]
			FROM	dbm.db_model_get_triggers AS G
			WHERE	(G.tname = @name
				OR @name IS NULL)
				AND G.schname = @schname
			ORDER BY
				G.[tname]

	END
	ELSE IF @source = 1 AND dbm.db_model_exists_table_column('dbm', 'db_model_saved_triggers', NULL) = 1
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[name],
			[text]
		)
			SELECT	DISTINCT
				[schname],
				[tname],
				[ttext]
			FROM	dbm.db_model_saved_triggers AS G
			WHERE	(G.tname = @name
				OR @name IS NULL)
				AND G.schname = @schname
			ORDER BY
				G.[tname]
	END

	DECLARE @ischname sysname
	DECLARE @iname sysname
	DECLARE @itext varchar(MAX)
	DECLARE @tmpResult varchar(MAX) = ''
	DECLARE @nl varchar(2) = CHAR(13) + CHAR(10)
		
	DECLARE generateObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[name],
			O.[text]
		FROM	#db_objects AS O
		ORDER BY
			O.[name]
	
	OPEN generateObjects
	
	FETCH NEXT FROM generateObjects INTO @ischname, @iname, @itext
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @tmpResult = ''
		SET @tmpResult = @tmpResult + 'IF dbm.db_model_exists_trigger(''' + @ischname + ''', ''' + @iname + ''') = 1' + @nl
		SET @tmpResult = @tmpResult + 'BEGIN' + @nl
		SET @tmpResult = @tmpResult + '	DROP TRIGGER [' + @ischname + '].[' + @iname + ']' + @nl
		SET @tmpResult = @tmpResult + 'END' + @nl
		INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
		INSERT INTO #result(batchend, code) VALUES(1, @itext + @nl)
		FETCH NEXT FROM generateObjects INTO @ischname, @iname, @itext
	END

	CLOSE generateObjects
	DEALLOCATE generateObjects

	IF @output = 0
	BEGIN
		SELECT	*
		FROM	#result
	END
	ELSE IF @output = 1
	BEGIN
		EXEC dbm.db_model_print_output '#result'
	END
	ELSE IF @output = 2
	BEGIN
		EXEC dbm.db_model_execute_output '#result'
	END

END
