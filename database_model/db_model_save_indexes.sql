EXEC dbm.db_model_utl_createProc 'db_model_save_indexes'
GO

ALTER PROCEDURE dbm.db_model_save_indexes
(
	@schname sysname,
	@kname sysname = NULL,
	@oname sysname = NULL,
	@columnList TableOfColumns READONLY -- not nullable, must be provided, but can be empty
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_indexes 'dbo', 'Provider_ImageBatch_FK', NULL, DEFAULT
DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('Transaction_id', 1), ('TransactionSign', 2)
EXEC dbm.db_model_save_indexes 'dbo', NULL, 'TransactionNegotiatedLoadComponentReduction', @t
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_indexes 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_indexes
DROP TABLE dbm.db_model_saved_indexes
SELECT * FROM dbm.db_model_get_indexes
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @obj AS TABLE
	(
		kname sysname,
		oname sysname
	)

	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	BEGIN
		WITH CList AS
		(
			SELECT	U.oname,
				U.iname AS kname,
				U.cname,
				U.keyno AS Pos
			FROM	dbm.db_model_get_indexes AS U
			WHERE	U.oname = @oname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, oname)
			SELECT
				C1.kname,
				C1.oname
			FROM
			(
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @columnList AS T ON T.ColumnName = U.cname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.oname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.oname
				) AS C2	ON C1.kname = C2.kname
					AND C1.oname = C2.oname
					AND C1.CNT = C2.CNT
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @columnList)
	END	

	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_indexes', NULL) = 0 
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_indexes
		FROM	dbm.db_model_get_indexes AS G
		WHERE
			(
			G.iname = @kname
				AND G.oname = @oname
				AND EXISTS (
					SELECT	1
					FROM	@obj AS CL
					WHERE	CL.kname = G.iname
						AND CL.oname = G.oname
					)
			OR @kname IS NULL
				AND G.oname = @oname
				AND EXISTS (
					SELECT	1
					FROM	@obj AS CL
					WHERE	CL.kname = G.iname
						AND CL.oname = G.oname
					)
			OR G.iname = @kname
				AND @oname IS NULL
				AND NOT EXISTS (SELECT 1 FROM @columnList)
			OR @kname IS NULL
				AND @oname IS NULL
				AND NOT EXISTS (SELECT 1 FROM @columnList)
			)
			AND G.schname = @schname
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_indexes
			SELECT	*
			FROM	dbm.db_model_get_indexes AS G
			WHERE
				(
				G.iname = @kname
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.iname
							AND CL.oname = G.oname
						)
				OR @kname IS NULL
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.iname
							AND CL.oname = G.oname
						)
				OR G.iname = @kname
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				OR @kname IS NULL
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				)
				AND G.schname = @schname
	END
END
