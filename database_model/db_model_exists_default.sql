EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_default'
GO

ALTER FUNCTION dbm.db_model_exists_default
(
	@schname sysname,
	@dname sysname,
	@oname sysname,
	@cname sysname
) RETURNS smallint
AS BEGIN

/*
SELECT * FROM dbm.db_model_get_defaults
SELECT dbm.db_model_exists_default('DF__Payment__CashFlo__704E565C', 'Payment', 'CashFlowDictionary_id', 'dbo')
SELECT dbm.db_model_exists_default('dlj', 'Transaction', 'TransactionAmountKind', 'dbo')
SELECT dbm.db_model_exists_default('dlj', 'DESC', NULL, 'dbo')
SELECT dbm.db_model_exists_default(null, 'ETLCommit', 'fileoffset', 'dbo')
SELECT dbm.db_model_exists_default('D_TestDefault', 'Customer', NULL, 'dbo')
SELECT dbm.db_model_exists_default('D_TestDefault', 'CustomerKind', NULL, 'dbo')
*/


	DECLARE @result smallint
	DECLARE @idname sysname
	DECLARE @ioname sysname
	DECLARE @icname sysname
	
	IF
	(
		@dname IS NULL
		AND @oname IS NULL
	)
	OR
	(
		@dname IS NOT NULL
		AND @oname IS NULL
		AND @cname IS NOT NULL
	)
		OR
	(	
		@schname IS NULL	
	)		
	BEGIN
		SET @result = -1
		RETURN @result
	END
	
	SELECT	TOP 1
		@idname = G.dname,
		@ioname = g.oname,
		@icname = G.cname
	FROM	dbm.db_model_get_defaults AS G
	WHERE
		(
			G.[dname] = @dname AND @oname IS NULL
				OR (G.oname = @oname
					AND (G.cname = @cname OR G.cname IS NULL))
		)
		AND G.schname = @schname
		
	IF @dname IS NOT NULL
		AND @oname IS NULL
		AND @cname IS NULL
	BEGIN
		IF @idname = @dname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END	
	END

	IF @dname IS NULL
		AND @oname IS NOT NULL
		AND @cname IS NOT NULL
	BEGIN
		IF @ioname = @oname
			AND @icname = @cname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END	
	END	

	IF @dname IS NULL
		AND @oname IS NOT NULL
		AND @cname IS NULL
	BEGIN
		IF @ioname = @oname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END	
	END
	
	IF @dname IS NOT NULL
		AND @oname IS NOT NULL
		AND @cname IS NOT NULL
	BEGIN
		IF @ioname = @oname
			AND @icname = @cname
			AND @idname = @dname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	
	IF @dname IS NOT NULL
		AND @oname IS NOT NULL
		AND @cname IS NULL
	BEGIN
		IF @ioname = @oname
			AND @idname = @dname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END		
	
	RETURN @result
END
