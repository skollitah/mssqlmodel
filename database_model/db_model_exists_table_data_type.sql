EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_table_data_type'
GO

ALTER FUNCTION dbm.db_model_exists_table_data_type
(
	@schname sysname,
	@tname sysname,
	@columnList dbm.TableOfColumns READONLY -- not nullable, must be provided, but can be empty
) RETURNS smallint
AS BEGIN

/*
SELECT * FROM dbm.db_model_get_table_data_types
DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('ColumnName', 1), ('ColumnPos', 2)
SELECT dbm.db_model_exists_table_data_type('TableOfColumns', @t, 'dbm')

DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('ColumnName', 1), ('ColumnPos', 2), ('ColumnPos', 3)
SELECT dbm.db_model_exists_table_data_type('TableOfColumns2', @t, 'dbm')

DECLARE @t AS dbm.TableOfColumns
SELECT dbm.db_model_exists_table_data_type('TableOfColumns', @t, 'dbm')

SELECT dbm.db_model_exists_table_data_type('TableOfColumns', DEFAULT, 'dbm')

SELECT dbm.db_model_exists_table_data_type(NULL, DEFAULT, NULL)
*/
	DECLARE @result smallint
	DECLARE @tableTmp AS dbm.TableOfColumns
	DECLARE @tableCmp AS dbm.TableOfColumns
	DECLARE @itname sysname
	
	IF
	(
		@tname IS NULL
		OR @schname IS NULL
	)
	BEGIN
		SET @result = -1
		RETURN @result
	END
	
	SET @itname = 
	(
		SELECT	TOP 1
			G.[tname]
		FROM	dbm.db_model_get_table_data_types AS G
		WHERE	G.[tname] = @tname
			AND G.schname = @schname
	)
	
	IF @tname IS NOT NULL
		AND NOT EXISTS (SELECT 1 FROM @columnList)
	BEGIN
		IF @itname IS NOT NULL
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	
	IF @tname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	BEGIN
		INSERT INTO @tableTmp(ColumnName, ColumnPos)
			SELECT	U.cname,
				U.colorder
			FROM	dbm.db_model_get_table_data_types AS U
			WHERE	U.tname = @tname
				AND U.schname = @schname

		INSERT INTO @tableCmp(ColumnName, ColumnPos)
			(SELECT ColumnName, ColumnPos FROM @columnList
			EXCEPT
			SELECT ColumnName, ColumnPos FROM @tableTmp)
			UNION ALL
			(SELECT ColumnName, ColumnPos FROM @tableTmp
			EXCEPT
			SELECT ColumnName, ColumnPos FROM @columnList)
			
		IF NOT EXISTS
		(
			SELECT	1
			FROM	@tableCmp
		)
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END			

	END
	RETURN @result
end
