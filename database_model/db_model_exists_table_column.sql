EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_table_column'
GO

ALTER FUNCTION dbm.db_model_exists_table_column
(
	@schname sysname,
	@tname sysname,
	@cname sysname
) RETURNS smallint
AS BEGIN

/*
SELECT dbm.db_model_exists_table_column('dbo', RequiredAdditionalDocument', 'RequiredAdditionalDocument_id')
SELECT dbm.db_model_exists_table_column('dbo', null, 'RequiredAdditionalDocument_id')
SELECT dbm.db_model_exists_table_column('dbo', 'RequiredAdditionalDocument', NULL')
SELECT * FROM db_model_get_tables_columns
*/
	DECLARE @result smallint
	
	IF @tname IS NULL
		OR @schname IS NULL
	BEGIN
		SET @result = -1
		RETURN @result
	END
	
	IF EXISTS
	(
		SELECT	1 
		FROM	dbm.db_model_get_tables_columns AS G
		WHERE	G.[oname] = @tname
			AND (G.[cname] = @cname OR @cname IS NULL)
			AND G.schname = @schname
	)
	BEGIN
		SET @result = 1
	END
	ELSE
	BEGIN
		SET @result = 0
	END
	
	RETURN @result
END
