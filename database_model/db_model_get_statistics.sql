EXEC dbm.db_model_utl_createView 'db_model_get_statistics'
GO

ALTER VIEW dbm.db_model_get_statistics
AS
-- SELECT * FROM dbm.db_model_get_statistics
	SELECT
			PO.[name] as [oname],
			SI.[name] as [sname],
			C.[name] as [cname],
			SIC.stats_column_id as [keyno],
			CASE
				WHEN no_recompute = 1 THEN 'Y'
				ELSE 'N'
			END AS no_rec,
			CASE
				WHEN has_filter = 1 THEN filter_definition
				ELSE NULL
			END AS filter,
			SM.name AS schname
	FROM	sys.objects AS PO
			INNER JOIN sys.stats AS SI ON PO.object_id = SI.object_id
				AND PO.type = 'U'
				AND SI.user_created = 1
			INNER JOIN sys.stats_columns AS SIC ON SI.stats_id = SIC.stats_id
				AND SI.object_id = SIC.object_id
			INNER JOIN sys.columns AS C ON SIC.column_id = C.column_id
				AND SIC.object_id = C.object_id
			INNER JOIN sys.schemas AS SM ON SM.schema_id = PO.schema_id

