EXEC dbm.db_model_utl_createProc 'db_model_save_tables_columns'
GO

ALTER PROCEDURE dbm.db_model_save_tables_columns
(
	@schname sysname,
	@oname sysname = NULL,
	@cname sysname = NULL
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_tables_columns 'dbo', 'RequiredAdditionalDocument', 'RequiredAdditionalDocument_id'
EXEC dbm.db_model_save_tables_columns 'dbo','AccountDistributor', NULL
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_tables_columns 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_tables_columns
DROP TABLE dbm.db_model_saved_tables_columns
SELECT * FROM dbm.db_model_get_tables_columns
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_tables_columns', NULL) = 0 
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_tables_columns
		FROM	dbm.db_model_get_tables_columns AS G
		WHERE	
			(
			G.oname = @oname
				AND G.cname = @cname
			OR G.oname = @oname
				AND @cname IS NULL
			OR @oname IS NULL
				AND @cname IS NULL
			)
			AND G.schname = @schname
				
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_tables_columns
			SELECT	*
			FROM	dbm.db_model_get_tables_columns AS G
			WHERE	
				(
				G.oname = @oname
					AND G.cname = @cname
				OR G.oname = @oname
					AND @cname IS NULL
				OR @oname IS NULL
					AND @cname IS NULL
				)
				AND G.schname = @schname
	END
END
