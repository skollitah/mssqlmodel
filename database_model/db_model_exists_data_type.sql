EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_data_type'
GO

ALTER FUNCTION dbm.db_model_exists_data_type
(	
	@schname sysname,
	@name sysname
) RETURNS smallint
AS BEGIN

/*
SELECT dbm.db_model_exists_data_type('AddressComponentLength', 'dbo')
SELECT dbm.db_model_exists_data_type(null, null)
SELECT * FROM dbm.db_model_get_data_types
*/

	DECLARE @result smallint

	IF @name IS NULL
		OR @schname IS NULL
	BEGIN
		SET @result = -1
		RETURN @result
	END	
	
	IF EXISTS
	(
		SELECT	1 
		FROM	dbm.db_model_get_data_types AS G
		WHERE	G.[utname] = @name
			AND G.schname = @schname
	)
	BEGIN
		SET @result = 1
	END
	ELSE
	BEGIN
		SET @result = 0
	END
	
	RETURN @result
END
