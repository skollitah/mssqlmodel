EXEC dbm.db_model_utl_createView 'db_model_get_data_types'
go

ALTER VIEW dbm.db_model_get_data_types
AS
-- select * from dbm.db_model_get_data_types where alterable = 'N'
	SELECT	
		t1.[name] AS[utname],
		CASE
			WHEN t2.[name] IN ('decimal', 'numeric') THEN t2.name + '(' + CONVERT(varchar(5), t1.precision) + ',' + CONVERT(varchar(5), t1.scale) + ')'
			WHEN t2.[name] IN ('char', 'nchar', 'varchar', 'nvarchar') AND t1.max_length <> -1 THEN t2.name + '(' + CONVERT(varchar(5), t1.max_length) + ')'
			WHEN t2.[name] IN ('char', 'nchar', 'varchar', 'nvarchar') AND t1.max_length = -1 THEN t2.name + '(MAX)'			
			ELSE t2.name
		END AS [stname],
		CASE
			WHEN t1.is_nullable = 0 THEN 'N'
			WHEN t1.is_nullable = 1 THEN 'Y'
		END AS [nullable],
		CASE 
			WHEN t2.[name] IN ('image', 'text', 'ntext') THEN 'N'
			ELSE 'Y'
		END AS [alterable],
		SM.name AS schname
	FROM sys.types AS t1
		INNER JOIN sys.types AS t2 ON t1.[system_type_id] = t2.[user_type_id]
			AND t1.is_user_defined =  1
			AND t1.is_table_type = 0
		INNER JOIN sys.schemas AS SM ON SM.schema_id = t1.schema_id	

