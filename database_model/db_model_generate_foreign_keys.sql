EXEC dbm.db_model_utl_createProc 'db_model_generate_foreign_keys'
GO

ALTER PROCEDURE dbm.db_model_generate_foreign_keys
(
	@schname sysname,
	@source bit, -- 0 - database, 1 - saved objects
	@output smallint, -- 0 - table, 1 - console, 2 - execute
	@leavExp bit = 0, -- 0 - rename all, 1 - leave explicitly defined names of keys 
	@kname sysname = NULL,
	@foname sysname = NULL,
	@fColumnList dbm.TableOfColumns READONLY, -- not nullable, must be provided, but can be empty
	@roname sysname = NULL,
	@rColumnList dbm.TableOfColumns READONLY -- not nullable, must be provided, but can be empty		
)
AS
BEGIN

/*
EXEC dbm.db_model_save_foreign_keys 'dbo'
DROP TABLE dbm.db_model_saved_foreign_keys

EXEC dbm.db_model_generate_foreign_keys 'dbo', 1, 0, 0
EXEC dbm.db_model_generate_foreign_keys 'dbo', 0, 1, 0
EXEC dbm.db_model_generate_foreign_keys 'dbo', 0, 1, 1

DECLARE @tf AS dbm.TableOfColumns
DECLARE @tr AS dbm.TableOfColumns
INSERT INTO @tf (ColumnName, ColumnPos) VALUES ('ShareInShareClass_Fund_id', 1), ('ShareInShareClass_ShareClass_id', 2)
INSERT INTO @tr (ColumnName, ColumnPos) VALUES ('Fund_id', 1), ('ShareClass_id', 2)
EXEC dbm.db_model_generate_foreign_keys 'dbo', 0, 2, 1, 'FK_Blockade_ShareInShareClass', 'Blockade', @tf, 'ShareInShareClass', @tr


SELECT * FROM dbm.db_model_get_foreign_keys
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @sql varchar(1000)
	DECLARE @obj AS TABLE
	(
		kname sysname,
		foname sysname,
		roname sysname
	)
	
	IF @foname IS NOT NULL AND @roname is NOT NULL
		AND EXISTS (SELECT 1 FROM @fColumnList) AND EXISTS (SELECT 1 FROM @rColumnList)
	BEGIN
		WITH CList AS
		(
			SELECT
				U.foname,
				U.fcname,
				U.roname,
				U.rcname,
				U.kname,
				U.pos AS Pos
			FROM	dbm.db_model_get_foreign_keys AS U
			WHERE	U.foname = @foname
				AND U.roname = @roname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, foname, roname)
			SELECT
				C1.kname,
				C1.foname,
				C1.roname
			FROM
			(
				SELECT	
					U.kname,
					U.foname,
					U.roname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @fColumnList AS T ON T.ColumnName = U.fcname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.foname,
						U.roname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.foname,
					U.roname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @rColumnList AS T ON T.ColumnName = U.rcname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.foname,
						U.roname
				) AS C2 ON C1.kname = C2.kname
					AND C1.foname = C2.foname
					AND C1.roname = C2.roname
					AND C1.CNT = C2.CNT		
				INNER JOIN (
				SELECT	
					U.kname,
					U.foname,
					U.roname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.foname,
						U.roname
				) AS C3	ON C1.kname = C3.kname
					AND C1.foname = C3.foname
					AND C1.roname = C3.roname
					AND C1.CNT = C3.CNT		
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @fColumnList)
				AND C1.CNT = (SELECT COUNT(*) FROM @rColumnList)
	END	

	IF OBJECT_ID('tempdb..#result') IS NOT NULL
	BEGIN
		DROP TABLE #result
	END
	
	CREATE TABLE #result
	(
		[row] numeric(10) identity(1,1),
		[batchend] bit,
		[code] varchar(MAX),
	)	

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1,1),
		[schname] sysname NULL,	
		[kname] sysname NULL,		
		[foname] sysname NULL,
		[fcname] sysname NULL,
		[roname] sysname NULL,
		[rcname] sysname NULL,
		[keyno] smallint NULL,
		[explicite] varchar(2)
	)


	IF @source = 0
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[kname],
			[foname],
			[fcname],
			[roname],
			[rcname],
			[keyno],
			[explicite]
		)
			SELECT
				G.[schname],
				G.[kname],
				G.[foname],
				G.[fcname],
				G.[roname],
				G.[rcname],
				G.pos AS [keyno],
				G.ExplicitDef
			FROM	dbm.db_model_get_foreign_keys AS G
			WHERE
				(
				G.kname = @kname
					AND G.foname = @foname
					AND G.roname = @roname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.kname
							AND CL.foname = G.foname
							AND CL.roname = G.roname
						)
				OR @kname IS NULL
					AND G.foname = @foname
					AND G.roname = @roname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.kname
							AND CL.foname = G.foname
							AND CL.roname = G.roname
						)
				OR G.kname = @kname
					AND @foname IS NULL
					AND @roname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @fColumnList)
					AND NOT EXISTS (SELECT 1 FROM @rColumnList)	
				OR @kname IS NULL
					AND @foname IS NULL
					AND @roname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @fColumnList)
					AND NOT EXISTS (SELECT 1 FROM @rColumnList)	
				)
				AND G.schname = @schname
			ORDER BY
				G.[foname],
				G.[fcname],
				G.[pos]
	END
	ELSE IF @source = 1 AND dbm.db_model_exists_table_column('dbm', 'db_model_saved_foreign_keys', NULL) = 1
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[kname],
			[foname],
			[fcname],
			[roname],
			[rcname],
			[keyno],
			[explicite]
		)
			SELECT
				G.[schname],
				G.[kname],
				G.[foname],
				G.[fcname],
				G.[roname],
				G.[rcname],
				G.pos AS [keyno],
				G.ExplicitDef
			FROM	dbm.db_model_saved_foreign_keys AS G
			WHERE
				(
				G.kname = @kname
					AND G.foname = @foname
					AND G.roname = @roname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.kname
							AND CL.foname = G.foname
							AND CL.roname = G.roname
						)
				OR @kname IS NULL
					AND G.foname = @foname
					AND G.roname = @roname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.kname
							AND CL.foname = G.foname
							AND CL.roname = G.roname
						)
				OR G.kname = @kname
					AND @foname IS NULL
					AND @roname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @fColumnList)
					AND NOT EXISTS (SELECT 1 FROM @rColumnList)	
				OR @kname IS NULL
					AND @foname IS NULL
					AND @roname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @fColumnList)
					AND NOT EXISTS (SELECT 1 FROM @rColumnList)	
				)
				AND G.schname = @schname
			ORDER BY
				G.[foname],
				G.[fcname],
				G.[pos]
	END
	
	DECLARE @ischname sysname	
	DECLARE @ikname sysname
	DECLARE @ifoname sysname
	DECLARE @ifcname sysname
	DECLARE @ironame sysname
	DECLARE @ircname sysname
	DECLARE @iexplicite varchar(2)
	DECLARE @indName varchar(2000)
	DECLARE @tmpResult varchar(8000) = ''
	DECLARE @fcolumns varchar(4000) = ''
	DECLARE @rcolumns varchar(4000) = ''
	DECLARE @ikeyno sysname
	DECLARE @nl varchar(2) = CHAR(13) + CHAR(10)

	DECLARE generateObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[kname],
			O.[foname],
			O.[roname],
			O.[explicite]			
		FROM	#db_objects AS O
		ORDER BY
			O.schname,
			O.[kname],
			O.[foname],
			O.[roname],
			O.[explicite]
	
	OPEN generateObjects
	
	FETCH NEXT FROM generateObjects INTO @ischname, @ikname, @ifoname, @ironame, @iexplicite
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		DECLARE generateColumns CURSOR FAST_FORWARD FOR
			SELECT
				O.[fcname],
				O.[rcname],
				O.[keyno]	
			FROM	#db_objects AS O
			WHERE	O.foname = @ifoname
				AND O.roname = @ironame
				AND O.kname = @ikname
				AND O.schname = @ischname
			ORDER BY
				O.[keyno]
		
		OPEN generateColumns
	
		SET @tmpResult = ''
		SET @tmpResult = 'DECLARE @tmpColsF AS dbm.TableOfColumns' + @nl
		SET @tmpResult = @tmpResult + 'DECLARE @tmpColsR AS dbm.TableOfColumns' + @nl
		SET @fcolumns = ''
		SET @rcolumns = ''
		SET @indName = @ifoname + '_'
		FETCH NEXT FROM generateColumns INTO @ifcname, @ircname, @ikeyno
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @fcolumns = @fcolumns + '[' + @ifcname + '],'
			SET @rcolumns = @rcolumns + '[' + @ircname + '],'
			SET @indName = @indName + @ifcname + '_'
			SET @tmpResult = @tmpResult + 'INSERT INTO @tmpColsF (ColumnName, ColumnPos) VALUES (''' + @ifcname + ''', ''' + @ikeyno + ''')' + @nl
			SET @tmpResult = @tmpResult + 'INSERT INTO @tmpColsR (ColumnName, ColumnPos) VALUES (''' + @ircname + ''', ''' + @ikeyno + ''')' + @nl
			FETCH NEXT FROM generateColumns INTO @ifcname, @ircname, @ikeyno
		END
		
		SET @fcolumns = LEFT(@fcolumns, LEN(@fcolumns) - 1)
		SET @rcolumns = LEFT(@rcolumns, LEN(@rcolumns) - 1)
		SET @indName = LEFT(@indName, 126) + 'FK'

		IF @leavExp = 1
		BEGIN
			IF @iexplicite = 'Y'
			BEGIN		
				SET @indName = @ikname
			END
		END	
				
		SET @tmpResult = @tmpResult + 'IF dbm.db_model_exists_foreign_key(''' + @ischname + ''', NULL, ''' + @ifoname + ''', @tmpColsF, ''' + @ironame + ''' , @tmpColsR) = 0' + @nl
		SET @tmpResult = @tmpResult + 'BEGIN' + @nl
		SET @tmpResult = @tmpResult + '	ALTER TABLE [' + @ischname + '].[' + @ifoname + '] ADD CONSTRAINT [' + @indName + '] FOREIGN KEY (' + @fcolumns  + ') REFERENCES [' + @ischname + '].[' + @ironame + '](' +  @rcolumns + ')' + @nl
		SET @tmpResult = @tmpResult + 'END' + @nl

		INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
		
		CLOSE generateColumns
		DEALLOCATE generateColumns	
		FETCH NEXT FROM generateObjects INTO @ischname, @ikname, @ifoname, @ironame, @iexplicite
	END

	IF @output = 0
	BEGIN
		SELECT	*
		FROM	#result
	END
	ELSE IF @output = 1
	BEGIN
		EXEC dbm.db_model_print_output '#result'
	END
	ELSE IF @output = 2
	BEGIN
		EXEC dbm.db_model_execute_output '#result'
	END
	
	CLOSE generateObjects
	DEALLOCATE generateObjects	
end
-- select * from db_model_get_checks
