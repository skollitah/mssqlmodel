EXEC dbm.db_model_utl_CreateView 'db_model_get_table_data_types'
GO

ALTER VIEW dbm.db_model_get_table_data_types
AS
-- SELECT * FROM dbm.db_model_get_table_data_types
	SELECT
		TT.[name] AS [tname],
		C.[name] AS [cname],
		ISNULL(dbt.utname,
		CASE
			WHEN t2.[name] IN ('decimal', 'numeric') THEN t2.name + '(' + CONVERT(varchar(5), c.precision) + ',' + CONVERT(varchar(5), c.scale) + ')'
			WHEN t2.[name] IN ('char', 'nchar', 'varchar', 'nvarchar') THEN t2.name + '(' + CONVERT(varchar(5), c.max_length) + ')'
			ELSE t2.name
		END) AS [utname],
		dbt.[stname],
		ISNULL(CASE
			WHEN c.is_nullable = 0 then 'N'
			WHEN c.is_nullable = 1 then 'Y'
		END, dbt.[nullable]) AS [nullable],
		c.collation_name AS COLLATION,
		c.column_id AS colorder,
		SM.name AS schname
	FROM	sys.table_types AS TT
		INNER JOIN sys.objects AS STO ON TT.type_table_object_id = STO.object_id
		INNER JOIN sys.columns AS C ON STO.[object_id] = C.[object_id]
			and STO.[type] = 'TT'
		INNER JOIN sys.types AS t1 ON c.[user_type_id] = t1.[user_type_id]
		INNER JOIN sys.types AS t2 ON t1.[system_type_id] = t2.[user_type_id]
		LEFT OUTER JOIN db_model_get_data_types dbt ON dbt.[utname] COLLATE database_default = t1.[name]
			AND t1.is_user_defined = 1
		INNER JOIN sys.schemas AS SM ON SM.schema_id = TT.schema_id