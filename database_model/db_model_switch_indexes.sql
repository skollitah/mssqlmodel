EXEC dbm.db_model_utl_createProc 'db_model_switch_indexes'
GO

ALTER PROCEDURE dbm.db_model_switch_indexes
(
	@schname sysname,
	@source bit, -- 0 - database, 1 - saved objects
	@output smallint, -- 0 - table, 1 - console, 2 - execute 
	@switch bit, -- 0 - disable, -- enable
	@kname sysname = NULL,
	@oname sysname = NULL,
	@columnList TableOfColumns READONLY
)
AS BEGIN

/*
EXEC dbm.db_model_save_indexes
DROP TABLE dbm.db_model_saved_indexes

EXEC dbm.db_model_switch_indexes 'dbo', 0, 1, 1

DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t(ColumnName, ColumnPos) VALUES ('Provider_id', 1)
EXEC dbm.db_model_switch_indexes 'dbo', 0, 1, 1, 'IDN_AcceptanceGroup_Provider_id', 'AcceptanceGroup', @t

SELECT * FROM dbm.db_model_get_indexes
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @obj AS TABLE
	(
		kname sysname,
		oname sysname
	)

	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	BEGIN
		WITH CList AS
		(
			SELECT	U.oname,
				U.iname AS kname,
				U.cname,
				U.keyno AS Pos
			FROM	dbm.db_model_get_indexes AS U
			WHERE	U.oname = @oname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, oname)
			SELECT
				C1.kname,
				C1.oname
			FROM
			(
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @columnList AS T ON T.ColumnName = U.cname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.oname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.oname
				) AS C2	ON C1.kname = C2.kname
					AND C1.oname = C2.oname
					AND C1.CNT = C2.CNT
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @columnList)
	END

	IF OBJECT_ID('tempdb..#result') IS NOT NULL
	BEGIN
		DROP TABLE #result
	END
	
	CREATE TABLE #result
	(
		[row] numeric(10) identity(1,1),
		[batchend] bit,
		[code] varchar(MAX)
	)

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1,1),
		[schname] sysname NULL,
		[oname] sysname NULL,
		[kname] sysname NULL,
		[cname] sysname NULL,
		[keyno] smallint NULL
	)

	IF @source = 0
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[oname],
			[kname],
			[cname],
			[keyno]
		)
			SELECT
				G.[schname],
				G.[oname],
				G.[iname],
				G.[cname],
				G.[keyno]
			FROM	dbm.db_model_get_indexes AS G
			WHERE
				(
				G.iname = @kname
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.iname
							AND CL.oname = G.oname
						)
				OR @kname IS NULL
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.iname
							AND CL.oname = G.oname
						)
				OR G.iname = @kname
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				OR @kname IS NULL
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				)
				AND G.schname = @schname
			ORDER BY
				[oname],
				[iname],
				[keyno]
	END
	ELSE IF @source = 1 AND dbm.db_model_exists_table_column('dbm', 'db_model_saved_indexes', NULL) = 1
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[oname],
			[kname],
			[cname],
			[keyno]
		)
			SELECT
				G.[schname],
				G.[oname],
				G.[iname],
				G.[cname],
				G.[keyno]
			FROM	dbm.db_model_saved_indexes AS G
			WHERE
				(
				G.iname = @kname
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.iname
							AND CL.oname = G.oname
						)
				OR @kname IS NULL
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.iname
							AND CL.oname = G.oname
						)
				OR G.iname = @kname
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				OR @kname IS NULL
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				)
				AND G.schname = @schname
			ORDER BY
				[oname],
				[iname],
				[keyno]
	END
	
	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @ikname sysname
	DECLARE @icname sysname
	DECLARE @ikeyno sysname
	DECLARE @iswitch varchar(10) =''
	DECLARE @tmpResult varchar(8000) = ''
	DECLARE @columns varchar(4000) = ''
	DECLARE @nl varchar(2) = CHAR(13) + CHAR(10)

	IF @switch = 0
	BEGIN
		SET @iswitch = 'DISABLE'
	END
	ELSE IF @switch = 1
	BEGIN
		SET @iswitch = 'REBUILD'
	END

	DECLARE generateObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[oname],
			O.[kname]		
		FROM	#db_objects AS O
		ORDER BY
			O.[oname]
	
	OPEN generateObjects
	
	FETCH NEXT FROM generateObjects INTO @ischname, @ioname, @ikname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE generateColumns CURSOR FAST_FORWARD FOR
			SELECT
				O.[cname],
				O.[keyno]	
			FROM	#db_objects AS O
			WHERE	O.oname = @ioname
				AND O.kname = @ikname
				AND O.schname = @ischname
			ORDER BY
				O.[keyno]
		
		OPEN generateColumns
		
		SET @tmpResult = ''
		SET @tmpResult = 'DECLARE @tmpCols AS dbm.TableOfColumns' + @nl
		SET @columns = ''
		FETCH NEXT FROM generateColumns INTO @icname, @ikeyno
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @columns = @columns + '[' + @icname + '],'
			SET @tmpResult = @tmpResult + 'INSERT INTO @tmpCols (ColumnName, ColumnPos) VALUES (''' + @icname + ''', ''' + @ikeyno + ''')' + @nl
			FETCH NEXT FROM generateColumns INTO @icname, @ikeyno
		END
		
		SET @columns = LEFT(@columns, LEN(@columns) - 1)		
		SET @tmpResult = @tmpResult + 'IF dbm.db_model_exists_index(''' + @ischname + ''', ''' + @ikname + ''', ''' + @ioname + ''', @tmpCols) = 1' + @nl
		SET @tmpResult = @tmpResult + 'BEGIN' + @nl
		SET @tmpResult = @tmpResult + '	ALTER INDEX [' +@ikname + '] ON [' + @ischname + '].[' + @ioname + '] ' + @iswitch + @nl
		SET @tmpResult = @tmpResult + 'END' + @nl
		INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
		
		CLOSE generateColumns
		DEALLOCATE generateColumns	
		
		FETCH NEXT FROM generateObjects INTO @ischname, @ioname, @ikname
	END
	
	IF @output = 0
	BEGIN
		SELECT	*
		FROM	#result
	END
	ELSE IF @output = 1
	BEGIN
		EXEC dbm.db_model_print_output '#result'
	END
	ELSE IF @output = 2
	BEGIN
		EXEC dbm.db_model_execute_output '#result'
	END
		
	CLOSE generateObjects
	DEALLOCATE generateObjects	
	
END
-- select * from db_model_get_checks
