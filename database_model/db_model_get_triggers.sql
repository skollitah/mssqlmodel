EXEC dbm.db_model_utl_createView 'db_model_get_triggers'
GO

ALTER VIEW dbm.db_model_get_triggers
AS
-- select * from dbm.db_model_get_triggers
	SELECT
		SP.[name] as [tname],
		rtrim(PO.[type]) as [ttype],
		c.[definition] as [ttext],
		SM.name AS schname
	FROM	sys.triggers AS SP
		INNER JOIN sys.objects AS PO ON SP.object_id = PO.object_id
			AND SP.type = 'TR'
		INNER JOIN sys.sql_modules AS C ON SP.object_id = c.object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = PO.schema_id
		
