EXEC dbm.db_model_utl_createView 'db_model_get_views'
GO

ALTER VIEW dbm.db_model_get_views
AS
-- select * from dbm.db_model_get_views
	SELECT	SVO.[name] AS [vname]
		,rtrim(SVO.[type]) AS [vtype]
		,C.[definition] AS [vtext]
		,SM.name AS [schname]
	FROM	sys.views AS SV
		INNER JOIN sys.objects AS SVO ON SV.object_id = SVO.object_id
			and SVO.[type] = 'V'
		INNER JOIN sys.sql_modules AS C ON SVO.object_id = c.object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = SVO.schema_id