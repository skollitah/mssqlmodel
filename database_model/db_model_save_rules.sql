EXEC dbm.db_model_utl_createProc 'db_model_save_rules'
GO

ALTER PROCEDURE dbm.db_model_save_rules
(
	@schname sysname,
	@kname sysname = NULL,
	@oname sysname = NULL,
	@cname sysname = NULL
	
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_rules 'dbo', 'R_AccountDividendStatusType', 'AccountDividendStatusType', NULL
EXEC dbm.db_model_save_rules 'dbo', NULL, 'Customer', 'Customer_idn'
EXEC dbm.db_model_save_rules 'dbo', NULL, 'FeeExemptionConditionType', NULL
EXEC dbm.db_model_save_rules 'dbo', 'xx', NULL, NULL
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_rules 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_rules
DROP TABLE dbm.db_model_saved_rules
SELECT * FROM dbm.db_model_get_rules
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_rules', NULL) = 0 
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_rules
		FROM	dbm.db_model_get_rules AS G
		WHERE
			(
			G.rname = @kname
				AND G.oname = @oname
				AND G.cname = @cname
			OR	G.rname = @kname
				AND G.oname = @oname
				AND G.cname IS NULL
				AND @cname IS NULL
			OR	G.rname = @kname
				AND @oname IS NULL
				AND @cname IS NULL					
			OR	@kname IS NULL
				AND G.oname = @oname
				AND G.cname IS NULL
				AND @cname IS NULL			
			OR	@kname IS NULL
				AND G.oname = @oname
				AND G.cname = @cname		
			OR @kname IS NULL
				AND @oname IS NULL
				AND @cname IS NULL
			)
			AND G.schname = @schname
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_rules
			SELECT	*
			FROM	dbm.db_model_get_rules AS G
			WHERE
				(
				G.rname = @kname
					AND G.oname = @oname
					AND G.cname = @cname
				OR	G.rname = @kname
					AND G.oname = @oname
					AND G.cname IS NULL
					AND @cname IS NULL
				OR	G.rname = @kname
					AND @oname IS NULL
					AND @cname IS NULL					
				OR	@kname IS NULL
					AND G.oname = @oname
					AND G.cname IS NULL
					AND @cname IS NULL			
				OR	@kname IS NULL
					AND G.oname = @oname
					AND G.cname = @cname		
				OR @kname IS NULL
					AND @oname IS NULL
					AND @cname IS NULL
				)
				AND G.schname = @schname
	END
END
