EXEC dbm.db_model_utl_createProc 'db_model_unbind_data_types'
GO

ALTER PROCEDURE dbm.db_model_unbind_data_types
(
	@schname sysname,
	@tname sysname = NULL,
	@oname sysname = NULL,
	@cname sysname = NULL
)
AS
BEGIN
	
/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_unbind_data_types 'dbo', NULL, 'DocumentContent', NULL
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_unbind_data_types NULL, 'ID', 'DocumentContent', NULL
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_unbind_data_types 'dbo', 'ID', NULL, NULL
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_unbind_data_types 'dbo', 'Sequence', 'DocumentContent', 'DocumentContentVersion'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_unbind_data_types 'dbo', NULL, 'PinValue', 'PinValue'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_unbind_data_types 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_get_tables_columns
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF
	
	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects
	(
		[id] numeric(10) identity(1,1),
		[schname] sysname NULL,
		[oname] sysname NULL,
		[cname] sysname NULL,
		[tname] sysname NULL,
		[nullable] varchar(10) NULL,
		[collation] sysname NULL
	)
		
	INSERT INTO #db_objects
	(
		[schname],
		[oname],
		[cname],
		[tname],
		[nullable],
		[collation]
	)
		SELECT
			G.[schname],
			G.[oname],
			G.[cname],
			G.[stname],
			CASE
				WHEN G.nullable = 'Y' THEN 'NULL'
				WHEN G.nullable = 'N' THEN 'NOT NULL'
			END,
			G.collation
		FROM	dbm.db_model_get_tables_columns AS G
		WHERE
			(
			G.[alterable] = 'Y'
			AND G.[stname] IS NOT NULL
			AND (
				G.[utname] = @tname
					AND G.oname = @oname
					AND G.cname = @cname
				OR G.[utname] = @tname
					AND @oname IS NULL
					AND @cname IS NULL
				OR @tname IS NULL
					AND G.oname = @oname
					AND G.cname = @cname
				OR G.[utname] = @tname
					AND G.oname = @oname
					AND @cname IS NULL
				OR @tname IS NULL
					AND G.oname = @oname
					AND @cname IS NULL					
				OR @tname IS NULL
					AND @oname IS NULL
					AND @cname IS NULL
				)
			)
			AND G.schname = @schname

	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @icname sysname
	DECLARE @itname sysname
	DECLARE @nullable varchar(10)
	DECLARE @collation sysname
	DECLARE @sql varchar(1000)
	
	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			O.[schname],
			O.[oname],
			O.[cname],
			O.[tname],
			O.[nullable],
			O.[collation]
		FROM	#db_objects AS O
	
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @icname, @itname, @nullable, @collation
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = 'ALTER TABLE [' + @ischname + '].[' + @ioname + '] ALTER COLUMN [' + @icname + '] ' + @itname + isnull(' collate ' +  @collation + ' ', ' ') + @nullable
		--PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @icname, @itname, @nullable, @collation
	END
	
	CLOSE dropObjects
	DEALLOCATE dropObjects
END
