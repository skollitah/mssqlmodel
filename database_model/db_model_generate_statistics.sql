EXEC dbm.db_model_utl_createProc 'db_model_generate_statistics'
GO

ALTER PROCEDURE dbm.db_model_generate_statistics
(
	@schname sysname,
	@source bit, -- 0 - database, 1 - saved objects
	@output smallint, -- 0 - table, 1 - console, 2 - execute 
	@kname sysname = NULL,
	@oname sysname = NULL,
	@columnList TableOfColumns READONLY
)
AS BEGIN

/*
EXEC dbm.db_model_save_statistics
DROP TABLE dbm.db_model_saved_statistics

EXEC dbm.db_model_generate_statistics 'dbo', 1, 0
EXEC dbm.db_model_generate_statistics 'dbo', 0, 0
EXEC dbm.db_model_generate_statistics 'dbo', 0, 1, 'ForeignIdentifierTemplate_ForeignIdentifier_FK'
DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t(ColumnName, ColumnPos) VALUES ('Sha_ShareLot_id', 1)
EXEC dbm.db_model_generate_statistics 'dbo', 0, 1, 'ShareLot_ShareLotSequence_Initial_FK', 'ShareLotSequence', @t
EXEC dbm.db_model_generate_statistics 'dbo', 0, 0, NULL, 'ShareLotSequence', @t

SELECT * FROM dbm.db_model_get_statistics
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @obj AS TABLE
	(
		kname sysname,
		oname sysname
	)

	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	BEGIN
		WITH CList AS
		(
			SELECT	U.oname,
				U.sname AS kname,
				U.cname,
				U.keyno AS Pos
			FROM	dbm.db_model_get_statistics AS U
			WHERE	U.oname = @oname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, oname)
			SELECT
				C1.kname,
				C1.oname
			FROM
			(
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @columnList AS T ON T.ColumnName = U.cname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.oname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.oname
				) AS C2	ON C1.kname = C2.kname
					AND C1.oname = C2.oname
					AND C1.CNT = C2.CNT
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @columnList)
	END

	IF OBJECT_ID('tempdb..#result') IS NOT NULL
	BEGIN
		DROP TABLE #result
	END
	
	CREATE TABLE #result
	(
		[row] numeric(10) identity(1,1),
		[batchend] bit,
		[code] varchar(MAX)
	)

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1,1),
		[schname] sysname NULL,
		[oname] sysname NULL,
		[kname] sysname NULL,
		[cname] sysname NULL,
		[keyno] smallint NULL,
		[reconpute] varchar(100) NULL,
		[filter] varchar(8000) NULL
	)

	IF @source = 0
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[oname],
			[kname],
			[cname],
			[keyno],
			[reconpute],
			[filter]
		)
			SELECT
				G.[schname],
				G.[oname],
				G.[sname],
				G.[cname],
				G.[keyno],
				CASE
					WHEN G.[no_rec] = 'Y' THEN 'WITH NORECOMPUTE'
					WHEN G.[no_rec] = 'N' THEN NULL
				END,
				'WHERE ' + G.filter
			FROM	dbm.db_model_get_statistics AS G
			WHERE
				(
				G.sname = @kname
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.sname
							AND CL.oname = G.oname
						)
				OR @kname IS NULL
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.sname
							AND CL.oname = G.oname
						)
				OR G.sname = @kname
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				OR @kname IS NULL
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				)
				AND G.schname = @schname
			ORDER BY
				[oname],
				[sname],
				[keyno]
	END
	ELSE IF @source = 1 AND dbm.db_model_exists_table_column('dbm', 'db_model_saved_statistics', NULL) = 1
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[oname],
			[kname],
			[cname],
			[keyno],
			[reconpute],
			[filter]
		)
			SELECT
				G.[schname],
				G.[oname],
				G.[sname],
				G.[cname],
				G.[keyno],
				CASE
					WHEN G.[no_rec] = 'Y' THEN 'WITH NORECOMPUTE'
					WHEN G.[no_rec] = 'N' THEN NULL
				END,
				'WHERE ' + G.filter
			FROM	dbm.db_model_saved_statistics AS G
			WHERE
				(
				G.sname = @kname
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.sname
							AND CL.oname = G.oname
						)
				OR @kname IS NULL
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.sname
							AND CL.oname = G.oname
						)
				OR G.sname = @kname
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				OR @kname IS NULL
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				)
				AND G.schname = @schname
			ORDER BY
				[oname],
				[sname],
				[keyno]
	END
	
	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @ikname sysname
	DECLARE @icname sysname
	DECLARE @ikeyno sysname
	DECLARE @irecompute varchar(100)
	DECLARE @ifilter varchar(8000)
	DECLARE @tmpResult varchar(MAX) = ''
	DECLARE @indName varchar(2000) = ''
	DECLARE @columns varchar(4000) = ''
	DECLARE @nl varchar(2) = CHAR(13) + CHAR(10)

	DECLARE generateObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[oname],
			O.[kname],
			O.[reconpute],
			O.[filter]			
		FROM	#db_objects AS O
		ORDER BY
			O.[oname]
	
	OPEN generateObjects
	
	FETCH NEXT FROM generateObjects INTO @ischname, @ioname, @ikname, @irecompute, @ifilter
	WHILE @@FETCH_STATUS = 0
	BEGIN
		DECLARE generateColumns CURSOR FAST_FORWARD FOR
			SELECT
				O.[cname],
				O.[keyno]	
			FROM	#db_objects AS O
			WHERE	O.oname = @ioname
				AND O.kname = @ikname
				AND O.schname = @ischname
			ORDER BY
				O.[keyno]
		
		OPEN generateColumns
		
		SET @tmpResult = ''
		SET @tmpResult = 'DECLARE @tmpCols AS dbm.TableOfColumns' + @nl
		SET @columns = ''
		SET @indName = @ioname + '_'
		FETCH NEXT FROM generateColumns INTO @icname, @ikeyno
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @columns = @columns + '[' + @icname + '],'
			SET @indName = @indName + @icname + '_'
			SET @tmpResult = @tmpResult + 'INSERT INTO @tmpCols (ColumnName, ColumnPos) VALUES (''' + @icname + ''', ''' + @ikeyno + ''')' + @nl
			FETCH NEXT FROM generateColumns INTO @icname, @ikeyno
		END
		
		SET @columns = LEFT(@columns, LEN(@columns) - 1)
		SET @indName = LEFT(@indName, 124)
		SET @indName = @indName + 'STAT'

		
		SET @tmpResult = @tmpResult + 'IF dbm.db_model_exists_statistic(''' + @ischname + ''', NULL, ''' + @ioname + ''', @tmpCols) = 0' + @nl
		SET @tmpResult = @tmpResult + 'BEGIN' + @nl
		SET @tmpResult = @tmpResult + '	CREATE STATISTICS [' + @indName + '] ON [' + @ischname + '].[' + @ioname + '](' + @columns + ') ' + ISNULL(@ifilter + ' ', '') + ISNULL(@irecompute, '') + @nl
		SET @tmpResult = @tmpResult + 'END' + @nl
		INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
		
		CLOSE generateColumns
		DEALLOCATE generateColumns	
		
		FETCH NEXT FROM generateObjects INTO @ischname, @ioname, @ikname, @irecompute, @ifilter
	END
	
	IF @output = 0
	BEGIN
		SELECT	*
		FROM	#result
	END
	ELSE IF @output = 1
	BEGIN
		EXEC dbm.db_model_print_output '#result'
	END
	ELSE IF @output = 2
	BEGIN
		EXEC dbm.db_model_execute_output '#result'
	END
		
	CLOSE generateObjects
	DEALLOCATE generateObjects	
	
END
-- select * from db_model_get_checks
