EXEC dbm.db_model_utl_createProc 'db_model_save_checks'
GO

ALTER PROCEDURE dbm.db_model_save_checks
(
	@schname sysname,
	@kname sysname = NULL,
	@oname sysname = NULL,
	@columnList dbm.TableOfColumns READONLY -- not nullable, must be provided, but can be empty
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_checks 'dbo', 'CK__DeemedDis__Statu__2C185C49', NULL, DEFAULT
DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('Approved', 1), ('Status', 2)
EXEC dbm.db_model_save_checks 'dbo', NULL, 'Domain', @t
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_checks 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_checks
DROP TABLE dbm.db_model_saved_checks
SELECT * FROM dbm.db_model_get_checks
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF
	
	DECLARE @obj AS TABLE
	(
		kname sysname,
		oname sysname
	)

	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	BEGIN
		WITH CList AS
		(
			SELECT	U.oname,
				U.kname,
				U.cname,
				U.pos AS Pos
			FROM	dbm.db_model_get_checks AS U
			WHERE	U.oname = @oname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, oname)
			SELECT
				C1.kname,
				C1.oname
			FROM
			(
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @columnList AS T ON T.ColumnName = U.cname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.oname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.oname
				) AS C2	ON C1.kname = C2.kname
					AND C1.oname = C2.oname
					AND C1.CNT = C2.CNT
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @columnList)
	END
	
	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_checks', NULL) = 0 
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_checks
		FROM	dbm.db_model_get_checks AS G
		WHERE
			(
			G.kname = @kname
				AND G.oname = @oname
				AND EXISTS (
					SELECT	1
					FROM	@obj AS CL
					WHERE	CL.kname = G.kname
						AND CL.oname = G.oname
					)
			OR @kname IS NULL
				AND G.oname = @oname
				AND EXISTS (
					SELECT	1
					FROM	@obj AS CL
					WHERE	CL.kname = G.kname
						AND CL.oname = G.oname
					)
			OR G.kname = @kname
				AND @oname IS NULL
				AND NOT EXISTS (SELECT 1 FROM @columnList)		
			OR @kname IS NULL
				AND @oname IS NULL
				AND NOT EXISTS (SELECT 1 FROM @columnList)
			)
			AND G.schname = @schname
				
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_checks
			SELECT	*
			FROM	dbm.db_model_get_checks AS G
			WHERE
				(
				G.kname = @kname
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.kname
							AND CL.oname = G.oname
						)
				OR @kname IS NULL
					AND G.oname = @oname
					AND EXISTS (
						SELECT	1
						FROM	@obj AS CL
						WHERE	CL.kname = G.kname
							AND CL.oname = G.oname
						)
				OR G.kname = @kname
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)		
				OR @kname IS NULL
					AND @oname IS NULL
					AND NOT EXISTS (SELECT 1 FROM @columnList)
				)
				AND G.schname = @schname
	END
END
