EXEC dbm.db_model_utl_createProc 'db_model_drop_rules'
GO

ALTER PROCEDURE dbm.db_model_drop_rules
(
	@schname sysname,
	@kname sysname = NULL,
	@oname sysname = NULL,
	@cname sysname = NULL
)
AS BEGIN

/*	
BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_rules 'dbo', NULL, 'Customer', 'CustomerActive'
--EXEC dbm.db_model_drop_rules 'dbo', NULL, 'AccountDividendStatusType'
--EXEC dbm.db_model_drop_rules 'dbo', 'rlj', 'Customer', 'Customer_idn'
--EXEC dbm.db_model_drop_rules 'dbo', 'R_AccountDividendStatusType', 'AccountDividendStatusType'
--EXEC dbm.db_model_drop_rules 'dbo', 'R_AccountDividendStatusType', NULL, NULL
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_rules 'dbo'
ROLLBACK TRAN modelupdate
SELECT * FROM dbm.db_model_get_rules
*/
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF
	
	IF
	(	
		@oname IS NULL
		AND @cname IS NOT NULL
	)
	BEGIN
		RAISERROR('Invalid  parameters', 16, 1)
		RETURN
	END

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1, 1),
		[schname] sysname NULL,
		[oname] sysname NULL,
		[cname] sysname NULL,
		[kname] sysname
	)
	
	INSERT INTO #db_objects
	(
		[schname],
		[oname],
		[cname],
		[kname]
	)
		SELECT
			DISTINCT
			G.[schname],
			G.oname,
			G.cname,
			G.rname
		FROM	dbm.db_model_get_rules AS G
		WHERE
			(
			G.oname = @oname
				AND G.cname = @cname
				AND G.rname = @kname
			OR G.oname = @oname
				AND G.cname = @cname
				AND @kname IS NULL
			OR G.oname = @oname
				AND @cname IS NULL
				AND @kname IS NULL			
			OR @oname IS NULL
				AND @cname IS NULL
				AND G.rname = @kname
			OR @oname IS NULL
				AND @cname IS NULL
				AND @kname IS NULL
			)
			AND G.[schname] = @schname

	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @icname sysname
	DECLARE @ikname sysname
	DECLARE @sql varchar(1000)

	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[oname],
			O.[cname]
		FROM	#db_objects AS O
		WHERE	O.oname IS NOT NULL
	
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @icname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @icname IS NOT NULL
		BEGIN
			SET @sql = ('EXEC sys.sp_unbindrule ''[' + @ischname + '].[' + @ioname + '].[' + @icname + ']''')
		END
		ELSE
		BEGIN
			SET @sql = ('EXEC sys.sp_unbindrule ''[' + @ischname + '].[' + @ioname + ']''')
		END
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ioname, @icname
	END

	CLOSE dropObjects
	DEALLOCATE dropObjects	

	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[kname]
		FROM	#db_objects AS O
	
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ikname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = ('DROP RULE [' + @ischname + '].[' + @ikname + ']')
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ikname
	END

	CLOSE dropObjects
	DEALLOCATE dropObjects
END
