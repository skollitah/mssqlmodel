EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_rule'
GO

ALTER FUNCTION dbm.db_model_exists_rule
(
	@schname sysname,
	@rname sysname,
	@oname sysname,
	@cname sysname
) RETURNS smallint
AS BEGIN

/*
SELECT * FROM dbm.db_model_get_rules
SELECT dbm.db_model_exists_rule('R_AccountDividendStatusType', 'AccountDividendStatusType', NULL, 'dbo')
SELECT dbm.db_model_exists_rule('rlj', 'Customer', 'Customer_idn', 'dbo')
SELECT dbm.db_model_exists_rule('rlj', 'lj_type', NULL, 'dbo')
SELECT dbm.db_model_exists_rule(NULL, NULL, NULL, NULL)
*/


	DECLARE @result smallint
	DECLARE @irname sysname
	DECLARE @ioname sysname
	DECLARE @icname sysname
	
	IF
	(
		@rname IS NULL
		AND @oname IS NULL
	)
	OR
	(
		@rname IS NOT NULL
		AND @oname IS NULL
		AND @cname IS NOT NULL
	)
		OR
	(	
		@schname IS NULL	
	)	
	BEGIN
		SET @result = -1
		RETURN @result
	END
	
	SELECT	TOP 1
		@irname = G.rname,
		@ioname = g.oname,
		@icname = G.cname
	FROM	dbm.db_model_get_rules AS G
	WHERE
		(
			G.[rname] = @rname AND @oname IS NULL
				OR (G.oname = @oname
					AND (G.cname = @cname OR G.cname IS NULL))
		)
		AND G.schname = @schname
		
	IF @rname IS NOT NULL
		AND @oname IS NULL
		AND @cname IS NULL
	BEGIN
		IF @irname = @rname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END	
	END

	IF @rname IS NULL
		AND @oname IS NOT NULL
		AND @cname IS NOT NULL
	BEGIN
		IF @ioname = @oname
			AND @icname = @cname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END	
	END	

	IF @rname IS NULL
		AND @oname IS NOT NULL
		AND @cname IS NULL
	BEGIN
		IF @ioname = @oname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END	
	END
	
	IF @rname IS NOT NULL
		AND @oname IS NOT NULL
		AND @cname IS NOT NULL
	BEGIN
		IF @ioname = @oname
			AND @icname = @cname
			AND @irname = @rname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	
	IF @rname IS NOT NULL
		AND @oname IS NOT NULL
		AND @cname IS NULL
	BEGIN
		IF @ioname = @oname
			AND @irname = @rname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END		
	
	RETURN @result
END
