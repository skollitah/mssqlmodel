EXEC dbm.db_model_utl_createProc 'db_model_generate_database'
GO

ALTER PROCEDURE dbm.db_model_generate_database
	@schname sysname,
	@source bit, -- 0 - database, 1 - saved objects
	@output smallint, -- 0 - table, 1 - console, 2 - execute
	@leavExp bit = 0 -- 0 - rename all, 1 - leave explicitly defined names of keys  
AS
BEGIN

/*
EXEC dbm.db_model_generate_database 'dbo', 0, 1, 0
EXEC dbm.db_model_generate_database 'dbo', 0, 0, 0
*/
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @tmpResult varchar(MAX) = ''
	DECLARE @row bigint = 1
	DECLARE @step int = 1
	DECLARE @nl varchar(2) = CHAR(13) + CHAR(10)

	IF OBJECT_ID('tempdb..#resulttmp') IS NOT NULL
	BEGIN
		DROP TABLE #resulttmp
	END
	
	CREATE TABLE #resulttmp
	(
		[row] numeric(10),
		[batchend] bit,
		[code] varchar(MAX)
	)	

	IF OBJECT_ID('tempdb..#resultAll') IS NOT NULL
	BEGIN
		DROP TABLE #resultAll
	END
	
	CREATE TABLE #resultAll
	(
		[row] numeric(10) PRIMARY KEY CLUSTERED,
		[batchend] bit,
		[code] varchar(MAX)
	)
	
	SET @tmpResult = '--BEGIN DATABASE GENERATION'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row, 0, @tmpResult)

	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP SAVED OBJECTS' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_all_saved' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP SAVED OBJECTS'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1	

	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ':  SAVE OBJECTS' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_save_all ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': SAVE OBJECTS'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1			

	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': UNBIND DATA TYPES' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_unbind_data_types ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': UNBIND DATA TYPES'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1
	
	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP CHECKS' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_checks ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP CHECKS'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1

	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP DEFAULTS' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_defaults ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP DEFAULTS'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1

	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP RULES' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_rules ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP RULES'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1	
		
	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP FOREIGN KEYS' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_foreign_keys ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP FOREIGN KEYS'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)				
	SET @step = @step + 1
	
	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP DATA TYPES' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_data_types ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP DATA TYPES'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1
	
	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP TABLE DATA TYPES' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_table_data_types ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP TALE DATA TYPES'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1
	
	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP UNIQUES' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_uniques ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP UNIQUES'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1
	
	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP INDEXES' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_indexes ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP INDEXES'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1			

	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP STATISTICS' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_statistics ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP STATISTICS'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1

	-- STEP BEGIN ----------------------------------
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': DROP PRIMARY KEYS' + @nl
	SET @tmpResult = @tmpResult + 'EXEC dbm.db_model_drop_primary_keys ''' + @schname + '''' + @nl
	SET @tmpResult = @tmpResult + '--END STEP ' + convert(varchar(3), @step) + ': DROP PRIMARY KEYS'  + @nl
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1		
	
	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE DATA TYPES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_data_types @schname, @source, 0
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE DATA TYPES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1
	
	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE TABLE DATA TYPES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_table_data_types @schname, @source, 0
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE TABLE DATA TYPES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1
	
	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE TABLES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_tables_columns @schname, @source, 0
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE TABLES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1

	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE RULES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_rules @schname, @source, 0, 1
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE RULES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1	

	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE DEFAULTS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_defaults @schname, @source, 0, 1, @leavExp
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE DEFAULTS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1	


	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE CHECKS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_checks @schname, @source, 0, @leavExp
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE CHECKS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1

	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE PRIMARY KEYS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_primary_keys @schname, @source, 0, @leavExp
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE PRIMARY KEYS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1			

	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE UNIQUES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_uniques @schname, @source, 0, @leavExp
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE UNIQUES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1

	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE INDEXES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_indexes @schname, @source, 0
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE INDEXES'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1

	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE STATISTICS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)	
	
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_statistics @schname, @source, 0
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE STATISTICS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1	

	-- STEP BEGIN ----------------------------------
	SET @step = @step + 1
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--BEGIN STEP ' + convert(varchar(3), @step) + ': GENERATE FOREIGN KEYS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 0, @tmpResult)
		
	SET @row = (SELECT MAX([row]) FROM #resultAll)
	TRUNCATE TABLE #resulttmp
	INSERT INTO #resulttmp ([row], [batchend], [code])
		EXEC dbm.db_model_generate_foreign_keys @schname, @source, 0, @leavExp
	INSERT INTO #resultAll ([row], [batchend], [code])
		SELECT	[row] + @row,
			[batchend],
			[code]
		FROM	#resulttmp

	SET @row = (SELECT MAX([row]) FROM #resultAll)
	SET @tmpResult = '--END STEP ' + convert(varchar(3), @step) + ': GENERATE FOREIGN KEYS'
	INSERT INTO #resultAll ([row], [batchend], [code])
		VALUES (@row + 1, 1, @tmpResult)
	SET @step = @step + 1

	IF @output = 0
	BEGIN
		SELECT	*
		FROM	#resultAll
	END
	ELSE IF @output = 1
	BEGIN
		EXEC dbm.db_model_print_output '#resultAll'
	END
	ELSE IF @output = 2
	BEGIN
		EXEC dbm.db_model_execute_output '#resultAll'
	END

end