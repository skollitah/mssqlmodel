EXEC dbm.db_model_utl_createProc 'db_model_save_data_types'
GO

ALTER PROCEDURE dbm.db_model_save_data_types
(
	@schname sysname,
	@name sysname = NULL
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_data_types 'dbo', 'Amount'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_data_types 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_data_types
DROP TABLE dbm.db_model_saved_data_types
SELECT * FROM dbm.db_model_get_data_types
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_data_types', NULL) = 0 
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_data_types
		FROM	dbm.db_model_get_data_types AS G
		WHERE	(G.utname = @name
			OR @name IS NULL)
			AND G.schname = @schname
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_data_types
			SELECT	*
			FROM	dbm.db_model_get_data_types AS G
			WHERE	(G.utname = @name
				OR @name IS NULL)
				AND G.schname = @schname
	END
END
