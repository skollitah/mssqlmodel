EXEC dbm.db_model_utl_createProc 'db_model_save_triggers'
GO

ALTER PROCEDURE dbm.db_model_save_triggers
(
	@schname sysname,
	@name sysname = NULL
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_triggers 'dbo', 'tr_FeeSessionOrderContext'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_triggers 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_triggers
DROP TABLE dbm.db_model_saved_triggers
SELECT * FROM dbm.db_model_get_triggers
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_triggers', NULL) = 0 
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_triggers
		FROM	dbm.db_model_get_triggers AS G
		WHERE	(G.tname = @name
			OR @name IS NULL)
			AND G.schname = @schname
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_triggers
			SELECT	*
			FROM	dbm.db_model_get_triggers AS G
			WHERE	(G.tname = @name
				OR @name IS NULL)
				AND G.schname = @schname
	END
END
