EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_view'
GO

ALTER FUNCTION dbm.db_model_exists_view
(
	@schname sysname,
	@name sysname
) RETURNS smallint
AS BEGIN

/*
SELECT dbm.db_model_exists_view('CustomerCharacteristics', 'dbo')
SELECT dbm.db_model_exists_view('CustomerCharacteristics', NULL)
SELECT dbm.db_model_exists_view('db_model_get_defaults', 'dbm')
SELECT * FROM dbm.db_model_get_views
*/

	DECLARE @result smallint

	IF @name IS NULL
		OR @schname IS NULL
	BEGIN
		SET @result = -1
		RETURN @result
	END	

	IF EXISTS
	(
		SELECT	1 
		FROM	dbm.db_model_get_views AS G
		WHERE	[vname] = @name
			AND G.schname = @schname
	)
	BEGIN
		SET @result = 1
	END
	ELSE
	BEGIN
		SET @result = 0
	END
	
	RETURN @result
END
