EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_index'
GO

ALTER FUNCTION dbm.db_model_exists_index
(
	@schname sysname,
	@kname sysname,
	@oname sysname = NULL,
	@columnList dbm.TableOfColumns READONLY -- not nullable, must be provided, but can be empty
) RETURNS smallint
AS BEGIN

/*
SELECT * FROM dbm.db_model_get_indexes
DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('ShareLot_id', 1), ('LoadComponent_id', 2)
SELECT dbm.db_model_exists_index('LoadComponentValue_ShareLot_id_LoadComponent_id_IDN', 'LoadComponentValue', @t, 'dbo')

DECLARE @t AS dbm.TableOfColumns
INSERT INTO @t (ColumnName, ColumnPos) VALUES ('ShareLot_id', 1), ('LoadComponent_id', 2)
SELECT dbm.db_model_exists_index(NULL, 'LoadComponentValue', @t, 'dbo')

DECLARE @t AS dbm.TableOfColumns
SELECT dbm.db_model_exists_index('LoadComponentValue_ShareLot_id_LoadComponent_id_IDN', NULL, @t, 'dbo')

SELECT dbm.db_model_exists_index('LoadComponentValue_ShareLot_id_LoadComponent_id_IDN', NULL, DEFAULT, 'dbo')

SELECT dbm.db_model_exists_index(NULL, NULL, DEFAULT, NULL)
*/
	DECLARE @result smallint
	DECLARE @cname sysname
	DECLARE @obj AS TABLE
	(
		kname sysname,
		oname sysname
	)
	
	IF
	(
		@oname IS NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	)
		OR
	(
		@oname IS NOT NULL
		AND NOT EXISTS (SELECT 1 FROM @columnList)
	)
		OR
	(
		@oname IS NULL
		AND NOT EXISTS (SELECT 1 FROM @columnList)
		AND @kname IS NULL
	)
		OR
	(	
		@schname IS NULL	
	)			
	BEGIN
		SET @result = -1
		RETURN @result
	END
	
	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
	BEGIN
		WITH CList AS
		(
			SELECT	U.oname,
				U.iname AS kname,
				U.cname,
				U.keyno AS Pos
			FROM	dbm.db_model_get_indexes AS U
			WHERE	U.oname = @oname
				AND U.schname = @schname
		)
		INSERT INTO @obj (kname, oname)
			SELECT
				C1.kname,
				C1.oname
			FROM
			(
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
					INNER JOIN @columnList AS T ON T.ColumnName = U.cname
						AND T.ColumnPos = U.Pos
				GROUP BY
						U.kname,
						U.oname
				) AS C1
				INNER JOIN (
				SELECT	
					U.kname,
					U.oname,
					COUNT(*) AS CNT	
				FROM	CList AS U
				GROUP BY
						U.kname,
						U.oname
				) AS C2	ON C1.kname = C2.kname
					AND C1.oname = C2.oname
					AND C1.CNT = C2.CNT
			WHERE	C1.CNT = (SELECT COUNT(*) FROM @columnList)
	END	
	
	SET @cname = 
	(
		SELECT	TOP 1
			G.[iname]
		FROM	dbm.db_model_get_indexes AS G
		WHERE	G.[iname] = @kname
			AND G.schname = @schname
	)
	
	IF @oname IS NULL
		AND NOT EXISTS (SELECT 1 FROM @columnList)
		AND @kname IS NOT NULL
	BEGIN
		IF @cname = @kname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	

	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
		AND @kname IS NULL
	BEGIN
		IF EXISTS
		(
			SELECT	1
			FROM	@obj
		)
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END
	

	IF @oname IS NOT NULL
		AND EXISTS (SELECT 1 FROM @columnList)
		AND @kname IS NOT NULL
	BEGIN
		IF EXISTS
		(
			SELECT	1
			FROM	@obj
		)
		AND @kname = @cname
		BEGIN
			SET @result = 1
		END
		ELSE
		BEGIN
			SET @result = 0
		END
	END	

	RETURN @result
end
