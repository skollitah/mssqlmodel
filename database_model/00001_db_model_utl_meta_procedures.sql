PRINT '-----------'
PRINT '-- utils --'
PRINT '-----------'
GO

--------------------------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM sys.objects AS so (NOLOCK) INNER JOIN sys.schemas AS ss (NOLOCK) on so.[schema_id] = ss.[schema_id] WHERE so.[object_id] = object_id(N'[dbm].[db_model_utl_GrantExecForProcToReporter]') AND [type]='P' and ss.name = 'dbm') BEGIN
  EXEC('CREATE PROCEDURE dbm.db_model_utl_GrantExecForProcToReporter (@procname sysname) AS SELECT 1')
  PRINT 'CREATE PROCEDURE dbm.db_model_utl_GrantExecForProcToReporter'
END
  PRINT 'ALTER PROCEDURE dbm.db_model_utl_GrantExecForProcToReporter'
go
alter procedure dbm.db_model_utl_GrantExecForProcToReporter( @procname sysname, @msg varchar(4000) output)
as
begin

  return

	declare @sql nvarchar(2000)

	set @sql = 'grant execute on ' + @procname + ' to [gtas_app]'
	set @msg = @msg + char(13)+ char(10) + @sql	
	exec(@sql)	

end
go

----
IF NOT EXISTS (SELECT * FROM sys.objects AS so (NOLOCK) INNER JOIN sys.schemas AS ss (NOLOCK) on so.[schema_id] = ss.[schema_id] WHERE so.[object_id] = object_id(N'[dbm].[db_model_utl_OperOnObj]') AND [type]='P' and ss.name = 'dbm') BEGIN
  EXEC('CREATE PROCEDURE dbm.db_model_utl_OperOnObj AS SELECT 1')
  PRINT 'CREATE PROCEDURE dbm.db_model_utl_OperOnObj'
END
  PRINT 'ALTER PROCEDURE dbm.db_model_utl_OperOnObj'
GO
ALTER PROCEDURE dbm.db_model_utl_OperOnObj (
  @Name		sysname
, @Xtype	char(2)
, @Oper		INT = 0
) AS BEGIN
DECLARE @msg VARCHAR(4000)

SET NOCOUNT ON

IF EXISTS (SELECT * FROM sys.objects WHERE name= @Name AND [type]= @Xtype) BEGIN
  	IF @xtype='P' 
	BEGIN
    	IF @Oper=1 BEGIN 
      		EXEC ('DROP PROCEDURE dbm.'+ @Name)
      		SELECT @msg= 'DROP PROCEDURE dbm.'+ @Name
    	END ELSE
		begin
      		SELECT @msg= 'ALTER PROCEDURE dbm.'+ @Name
			EXEC dbm.db_model_utl_GrantExecForProcToReporter @Name, @msg output
		end
  	END 
	ELSE IF @xtype='V' 
	BEGIN
	    IF @Oper=1 BEGIN 
	      EXEC ('DROP VIEW dbm.'+ @Name)
	      SELECT @msg= 'DROP VIEW dbm.'+ @Name
	    END ELSE
	      SELECT @msg= 'ALTER VIEW dbm.'+ @Name
	END
	ELSE IF  @xtype='FN' OR @xtype='IF' OR @xtype='TF'
	BEGIN
	    IF @Oper=1 BEGIN 
	      EXEC ('DROP FUNCTION dbm.'+ @Name)
	      SELECT @msg= 'DROP FUNCTION dbm.'+ @Name
	    END ELSE
		begin
	      	SELECT @msg= 'ALTER FUNCTION dbm.'+ @Name		
			if @xtype = 'FN'
			  	EXEC dbm.db_model_utl_GrantExecForProcToReporter @Name, @msg output
		end
	END
	ELSE 
	    PRINT '!!! db_model_utl_OperOnObj: @xtype = "' + @xtype + '"'
END ELSE BEGIN
  IF @Xtype='P' AND @Oper=0 BEGIN
    EXEC ('CREATE PROCEDURE dbm.'+@Name+ ' AS SELECT 1')
    SELECT @msg= 'CREATE PROCEDURE dbm.'+ @Name
	EXEC dbm.db_model_utl_GrantExecForProcToReporter @Name, @msg output
  END 
  IF @Xtype='V' AND @Oper=0 BEGIN
    EXEC ('CREATE VIEW dbm.'+@Name+ ' AS SELECT * FROM sys.objects')
    SELECT @msg= 'CREATE VIEW dbm.'+ @Name
  END 
  IF @Xtype='FN' AND @Oper=0 BEGIN
    EXEC ('CREATE FUNCTION dbm.'+@Name+ '() RETURNS INT AS BEGIN RETURN 1 END')
    SELECT @msg= 'CREATE FUNCTION dbm.'+ @Name
	EXEC dbm.db_model_utl_GrantExecForProcToReporter @Name, @msg output
  END 
  IF @Xtype='IF' AND @Oper=0 BEGIN
    EXEC ('CREATE FUNCTION dbm.'+@Name+ '() RETURNS TABLE AS RETURN SELECT * FROM sys.objects')
    SELECT @msg= 'CREATE FUNCTION dbm.'+ @Name
  END 
  IF @Xtype='TF' AND @Oper=0 BEGIN
    EXEC ('CREATE FUNCTION dbm.'+@Name+ '() RETURNS @t TABLE (a int) AS BEGIN RETURN END')
    SELECT @msg= 'CREATE FUNCTION dbm.'+ @Name
  END 

END
PRINT @msg
END
GO

EXEC dbm.db_model_utl_OperOnObj db_model_utl_CreateProc, P
GO
ALTER PROCEDURE dbm.db_model_utl_CreateProc (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'P', 0
END
GO

EXEC dbm.db_model_utl_OperOnObj db_model_utl_DropProc, P
GO
ALTER PROCEDURE dbm.db_model_utl_DropProc (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'P', 1
END
GO

EXEC dbm.db_model_utl_OperOnObj db_model_utl_CreateView, P
GO
ALTER PROCEDURE dbm.db_model_utl_CreateView (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'V', 0
END
GO

EXEC dbm.db_model_utl_OperOnObj db_model_utl_DropView, P
GO
ALTER PROCEDURE dbm.db_model_utl_DropView (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'V', 1
END
GO
-------
EXEC dbm.db_model_utl_OperOnObj db_model_utl_CreateFuncFN, P
GO
ALTER PROCEDURE dbm.db_model_utl_CreateFuncFN (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'FN', 0
END
GO

EXEC dbm.db_model_utl_OperOnObj db_model_utl_DropFuncFN , P
GO
ALTER PROCEDURE dbm.db_model_utl_DropFuncFN  (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'FN', 1
END
GO
-------
EXEC dbm.db_model_utl_OperOnObj db_model_utl_CreateFuncIF, P
GO
ALTER PROCEDURE dbm.db_model_utl_CreateFuncIF (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'IF', 0
END
GO

EXEC dbm.db_model_utl_OperOnObj db_model_utl_DropFuncIF , P
GO
ALTER PROCEDURE dbm.db_model_utl_DropFuncIF  (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'IF', 1
END
GO
-------
EXEC dbm.db_model_utl_OperOnObj db_model_utl_CreateFuncTF, P
GO
ALTER PROCEDURE dbm.db_model_utl_CreateFuncTF (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'TF', 0
END
GO

EXEC dbm.db_model_utl_OperOnObj db_model_utl_DropFuncTF , P
GO
ALTER PROCEDURE dbm.db_model_utl_DropFuncTF  (
  @Name		sysname
) AS BEGIN
EXEC dbm.db_model_utl_OperOnObj @Name, 'TF', 1
END
GO

