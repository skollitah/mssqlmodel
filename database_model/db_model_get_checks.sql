EXEC dbm.db_model_utl_createView 'db_model_get_checks'
GO

ALTER VIEW dbm.db_model_get_checks
AS
-- select * from dbm.db_model_get_checks
	SELECT
		OP.[name] as [oname],
		OC.[name] as [kname],
		COL.name AS [cname],
		ROW_NUMBER() OVER(PARTITION BY OP.name, OC.name ORDER BY CHARINDEX('[' + COL.name + ']', C.[definition])) AS pos,
		C.[definition] as [ctext],
		CASE
			WHEN C.is_system_named = 1 THEN 'N'
			ELSE 'Y'
		END AS ExplicitDef,
		SM.name AS schname
	FROM	sys.check_constraints AS C
		INNER JOIN sys.objects AS OC ON C.object_id = OC.object_id
		INNER JOIN sys.objects AS OP ON C.parent_object_id = OP.object_id
			AND OP.type = 'U'
		INNER JOIN sys.columns AS COL ON COL.object_id = OP.object_id
			AND CHARINDEX('[' + COL.name + ']', C.[definition]) > 0
		INNER JOIN sys.schemas AS SM ON SM.schema_id = OC.schema_id