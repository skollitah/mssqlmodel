EXEC dbm.db_model_utl_createView 'db_model_get_procedures'
GO

ALTER VIEW dbm.db_model_get_procedures
AS
-- select * from dbm.db_model_get_procedures
	SELECT
		SP.[name] as [pname],
		rtrim(PO.[type]) as [ptype],
		c.[definition] as [ptext],
		SM.name AS schname
	FROM	sys.procedures AS SP
		INNER JOIN sys.objects AS PO ON SP.object_id = PO.object_id
			AND SP.type = 'P'
		INNER JOIN sys.sql_modules AS C ON SP.object_id = c.object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = PO.schema_id