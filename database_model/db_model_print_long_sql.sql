EXEC dbm.db_model_utl_createProc 'db_model_print_long_sql'
GO

ALTER PROCEDURE dbm.db_model_print_long_sql
        @SQLInput nvarchar(MAX),
        @VariableName nvarchar(128)
AS
BEGIN

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

    DECLARE @VariableLength numeric(10,2)
    DECLARE @Chunk nvarchar(4000)
    DECLARE @SubstringStart int
    DECLARE @SubstringEnd int

    SET @VariableLength = LEN(@SQLInput)
    SET @SubstringStart = 0
    SET @SubstringEnd = 4000
    
    IF (@VariableLength <= @SubstringEnd)
    BEGIN
		PRINT @SQLInput
    END

    WHILE (@SubstringStart + @SubstringEnd) < @VariableLength
    BEGIN
        --FIX "BROKEN LINE AT 4,000 CHARACTER POSITION" PROBLEM.
        SELECT @SubstringStart = @SubstringStart + CASE @SubstringStart WHEN 0 THEN 1 ELSE @SubstringEnd END

        SET @Chunk = SUBSTRING(@SQLInput, @SubstringStart, 4000)
        IF RIGHT(@Chunk, 1) NOT IN ('', CHAR(10), CHAR(32), CHAR(9), CHAR(13))--IF THERE IS A LETTER IN THE 4,000th POSITION, ASSUME THAT IT'S A BROKEN LINE...
        BEGIN
            SET @SubstringEnd = LEN(@Chunk) - (CHARINDEX(CHAR(10), REVERSE(@Chunk))) --...AND STOP THE PRINT AT THE END OF THE PREVIOUS LINE.
        END
        ELSE
        BEGIN
            SET @SubstringEnd = LEN(@Chunk) --OTHERWISE, END POSITION IS OK.
        END

        PRINT SUBSTRING(@SQLInput, @SubstringStart, @SubstringEnd)
    END
END
GO