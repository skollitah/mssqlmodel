EXEC dbm.db_model_utl_createProc 'db_model_drop_data_types'
GO

ALTER PROCEDURE dbm.db_model_drop_data_types
(
	@schname sysname,
	@name sysname = NULL
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_data_types 'dbo', 'Password'
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_drop_data_types 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_get_data_types
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1, 1),
		[schname] sysname,
		[name] sysname
	)

	INSERT INTO #db_objects
	(
		[schname],
		[name]
	)
		SELECT
			DISTINCT
			G.[schname],
			G.utname
		FROM	dbm.db_model_get_data_types AS G
		WHERE	(G.utname = @name
			OR @name IS NULL)
			AND G.schname = @schname
	
	DECLARE @ischname sysname
	DECLARE @ioname sysname
	DECLARE @sql varchar(1000)
	
	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT
			O.[schname],
			O.[name]
		FROM	#db_objects AS O
	
	OPEN dropObjects
	
	FETCH NEXT FROM dropObjects INTO @ischname, @ioname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = ('DROP TYPE [' + @ischname + '].[' + @ioname + ']')
		PRINT (@sql)
 		EXEC (@sql)
 		FETCH NEXT FROM dropObjects INTO @ischname, @ioname
	END
	
	CLOSE dropObjects
	DEALLOCATE dropObjects
END
