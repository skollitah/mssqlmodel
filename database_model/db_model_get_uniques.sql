EXEC dbm.db_model_utl_createView 'db_model_get_uniques'
GO

ALTER VIEW dbm.db_model_get_uniques
AS
-- select * from dbm.db_model_get_uniques order by oname, kname, pos
	SELECT
		PKO.[name] as [kname],
		PO.[name] as [oname],
		C.[name] as [cname],
		SIC.key_ordinal as [pos],
		CASE
			WHEN SI.type = 1 THEN 'Y'
			WHEN SI.type = 2 THEN 'N'
		END AS [clustered],
		CASE
			WHEN PK.is_system_named = 1 THEN 'N'
			ELSE 'Y'
		END AS ExplicitDef,
		SM.name AS schname
	from	sys.key_constraints AS PK
		INNER JOIN sys.objects AS PKO ON PK.object_id = PKO.object_id
			AND PK.type = 'UQ'
		INNER JOIN sys.objects AS PO ON PK.[parent_object_id] = PO.object_id
			AND PO.type = 'U'
		INNER JOIN sys.indexes AS SI ON PO.object_id = SI.object_id
			AND PK.unique_index_id = SI.index_id
			AND SI.is_unique_constraint = 1
		INNER JOIN sys.index_columns AS SIC ON SI.index_id = SIC.index_id
			AND SI.object_id = SIC.object_id
		INNER JOIN sys.columns AS C ON SIC.column_id = C.column_id
			AND SIC.object_id = C.object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = PKO.schema_id

