EXEC dbm.db_model_utl_createFuncFN 'db_model_exists_function'
GO

ALTER FUNCTION dbm.db_model_exists_function
(
	@schname sysname,
	@name sysname
) RETURNS smallint
AS BEGIN

/*
SELECT dbm.db_model_exists_function('endChar', 'dbo')
SELECT dbm.db_model_exists_function(null, null)
SELECT * FROM dbm.db_model_get_functions
*/
	DECLARE @result smallint
	
	IF @name IS NULL
		OR @schname IS NULL
	BEGIN
		SET @result = -1
		RETURN @result
	END	
	
	IF EXISTS
	(
		SELECT	1 
		FROM	dbm.db_model_get_functions AS G
		WHERE	G.[fname] = @name
			AND G.schname = @schname
	)
	BEGIN
		SET @result = 1
	END
	ELSE
	BEGIN
		SET @result = 0
	END
	
	RETURN @result
END
