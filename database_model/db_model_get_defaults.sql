EXEC dbm.db_model_utl_createView 'db_model_get_defaults'
go

ALTER VIEW dbm.db_model_get_defaults
AS
-- select * from dbm.db_model_get_defaults
	SELECT
		DO.[name] as [dname],
		T.name AS [oname],
		CONVERT(sysname, NULL) as [cname],
		ISNULL(DC.[definition], SC.[definition]) as [dtext],
		CASE
			WHEN DC.is_system_named = 1 THEN 'N'
			ELSE 'Y'
		END AS ExplicitDef,
		CASE
			WHEN DO.parent_object_id = 0 THEN 'Y'
			ELSE 'N'
		END AS [Independent],
		SM.name AS schname
	FROM	sys.objects AS DO
		LEFT OUTER JOIN sys.sql_modules AS SC ON DO.object_id = SC.object_id
		LEFT OUTER JOIN sys.default_constraints AS DC ON DC.object_id = DO.object_id
		INNER JOIN sys.types AS T ON T.default_object_id = DO.object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = DO.schema_id
	WHERE	DO.type = 'D'
	UNION
	SELECT
		DO.[name] as [dname],
		PO.[name] as [oname],
		C.[name] as [cname],
		ISNULL(DC.[definition], SC.[definition]) as [dtext],
		CASE
			WHEN DC.is_system_named = 1 THEN 'N'
			ELSE 'Y'
		END AS ExplicitDef,
		CASE
			WHEN DO.parent_object_id = 0 THEN 'Y'
			ELSE 'N'
		END AS [Independent],
		SM.name AS schname		
	FROM	sys.objects AS DO
		LEFT OUTER JOIN sys.sql_modules AS SC ON DO.object_id = SC.object_id
		LEFT OUTER JOIN sys.default_constraints AS DC ON DC.object_id = DO.object_id
		INNER JOIN sys.columns AS C ON DO.object_id = C.default_object_id
		INNER JOIN sys.objects AS PO ON C.object_id = PO.object_id
			AND PO.type = 'U'
		INNER JOIN sys.types AS T ON C.user_type_id = T.user_type_id
			AND T.default_object_id <> DO.object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = DO.schema_id
	WHERE	DO.type = 'D'
	UNION
	SELECT
		DO.[name] as [dname],
		CONVERT(sysname, NULL) as [oname],
		CONVERT(sysname, NULL) as [cname],
		SC.[definition] as [dtext],
		'Y' AS ExplicitDef,
		CASE
			WHEN DO.parent_object_id = 0 THEN 'Y'
			ELSE 'N'
		END AS [Independent],
		SM.name AS schname		
	FROM	sys.objects AS DO
		INNER JOIN sys.sql_modules AS SC ON DO.object_id = SC.object_id
		LEFT OUTER JOIN sys.types AS T ON T.default_object_id = DO.object_id
		LEFT OUTER JOIN sys.columns AS C
			INNER JOIN sys.objects AS PO ON C.object_id = PO.OBJECT_ID
				AND PO.type = 'U'
		ON DO.object_id = C.default_object_id
		INNER JOIN sys.schemas AS SM ON SM.schema_id = DO.schema_id
	WHERE	DO.type = 'D'
		AND C.default_object_id IS NULL
		AND T.default_object_id IS NULL