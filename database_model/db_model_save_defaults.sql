EXEC dbm.db_model_utl_createProc 'db_model_save_defaults'
GO

ALTER PROCEDURE dbm.db_model_save_defaults
(
	@schname sysname,
	@kname sysname = NULL,
	@oname sysname = NULL,
	@cname sysname = NULL
	
)
AS BEGIN

/*
BEGIN TRAN modelupdate
EXEC dbm.db_model_save_defaults 'dbo', 'DF__shareinsh__Share__12E8C319', 'ShareInShareClass', 'ShareClassAccruingDelay'
EXEC dbm.db_model_save_defaults 'dbo', NULL, 'ShareInShareClass', 'EqualisationReferenceDate'
EXEC dbm.db_model_save_defaults 'dbo', NULL, 'ETLCommit', NULL
EXEC dbm.db_model_save_defaults 'dbo', 'dlj', NULL, NULL
ROLLBACK TRAN modelupdate

BEGIN TRAN modelupdate
EXEC dbm.db_model_save_defaults 'dbo'
ROLLBACK TRAN modelupdate

SELECT * FROM dbm.db_model_saved_defaults
DROP TABLE dbm.db_model_saved_defaults
SELECT * FROM dbm.db_model_get_defaults
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF dbm.db_model_exists_table_column('dbm', 'db_model_saved_defaults', NULL) = 0 
	BEGIN
		SELECT	*
		INTO	dbm.db_model_saved_defaults
		FROM	dbm.db_model_get_defaults AS G
		WHERE
			(
			G.dname = @kname
				AND G.oname = @oname
				AND G.cname = @cname
			OR	G.dname = @kname
				AND G.oname = @oname
				AND G.cname IS NULL
				AND @cname IS NULL
			OR	G.dname = @kname
				AND @oname IS NULL
				AND @cname IS NULL					
			OR	@kname IS NULL
				AND G.oname = @oname
				AND G.cname IS NULL
				AND @cname IS NULL			
			OR	@kname IS NULL
				AND G.oname = @oname
				AND G.cname = @cname		
			OR @kname IS NULL
				AND @oname IS NULL
				AND @cname IS NULL
			)
			AND G.schname = @schname
	END
	ELSE
	BEGIN
		INSERT INTO dbm.db_model_saved_defaults
			SELECT	*
			FROM	dbm.db_model_get_defaults AS G
			WHERE
				(
				G.dname = @kname
					AND G.oname = @oname
					AND G.cname = @cname
				OR	G.dname = @kname
					AND G.oname = @oname
					AND G.cname IS NULL
					AND @cname IS NULL
				OR	G.dname = @kname
					AND @oname IS NULL
					AND @cname IS NULL					
				OR	@kname IS NULL
					AND G.oname = @oname
					AND G.cname IS NULL
					AND @cname IS NULL			
				OR	@kname IS NULL
					AND G.oname = @oname
					AND G.cname = @cname		
				OR @kname IS NULL
					AND @oname IS NULL
					AND @cname IS NULL
				)
				AND G.schname = @schname
	END
END
