EXEC dbm.db_model_utl_createFuncTF 'db_model_utl_FindProcedureDependencies'
GO

ALTER FUNCTION dbm.db_model_utl_FindProcedureDependencies
(
	@ProcName varchar(256)
)
	RETURNS @blah TABLE (
		depth tinyint not NULL,
		tree varchar(7700),
		objectname varchar(256),
		dependencytype varchar(16)
	)
AS BEGIN

-- SELECT * FROM dbm.db_model_utl_FindProcedureDependencies('app_CD____Payments_AssignBankAccounts')

	DECLARE @depth tinyint
	SELECT @depth = 1

	INSERT @blah (depth, tree, objectname, dependencytype)
		SELECT	DISTINCT
			@depth,
			@ProcName + ' -> ' + SO2.name,
			SO2.name,
			'stored procedure'
		FROM	sys.objects AS SO1
			INNER JOIN sys.sql_modules AS SC1 ON SO1.object_id = SC1.object_id
			INNER JOIN sys.objects AS SO2 ON SO1.object_id <> SO2.object_id
			INNER JOIN sys.sql_modules AS SC2 ON SO2.object_id = SC2.object_id
		WHERE	SO1.type = 'P'
			AND SO2.type = 'P'
			AND SO1.name = @ProcName
			AND REPLACE(SC1.definition, SO1.name, '') LIKE '%EXEC%' + SO2.name + '%'
	
	WHILE (@@ROWCOUNT > 0)
	BEGIN
		SET @depth = @depth + 1
		INSERT @blah (depth, tree, objectname, dependencytype)
			SELECT	DISTINCT
				@depth,
				b.objectname + ' -> ' + so2.name,
				so2.name,
				'stored procedure'
			FROM 
				sys.objects AS SO1
				INNER JOIN sys.sql_modules AS SC1 ON SO1.object_id = SC1.object_id
				INNER JOIN sys.objects AS SO2 ON SO1.object_id <> SO2.object_id
				INNER JOIN sys.sql_modules AS SC2 ON SO2.object_id = SC2.object_id
				CROSS JOIN @blah b
			WHERE	so1.type = 'P'
				AND so2.type = 'P'
				AND so1.name collate database_default = b.objectname
				AND REPLACE(SC1.definition, SO1.name, '') LIKE '%EXEC%' + SO2.name + '%'
				AND b.depth = @depth - 1
	END

	RETURN
END

