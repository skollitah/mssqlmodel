EXEC dbm.db_model_utl_createProc 'db_model_drop_all_saved'
GO

ALTER PROCEDURE dbm.db_model_drop_all_saved
AS BEGIN

/*
EXEC dbm.db_model_drop_all_saved
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	DECLARE @ioname sysname
	DECLARE @sql varchar(2000)

	DECLARE dropObjects CURSOR FAST_FORWARD FOR
		SELECT	DISTINCT
			G.oname
		FROM	dbm.db_model_get_tables_columns AS G
		WHERE	G.oname LIKE 'db_model_saved%'
			AND G.schname = 'dbm'
		
	OPEN dropObjects
	FETCH NEXT FROM dropObjects INTO @ioname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql ='DROP TABLE dbm.[' + @ioname + ']'
		PRINT @sql
		EXEC(@sql)
		FETCH NEXT FROM dropObjects INTO @ioname
	END
	
	CLOSE dropObjects
	DEALLOCATE dropObjects
END
