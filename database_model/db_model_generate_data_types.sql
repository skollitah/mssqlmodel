EXEC dbm.db_model_utl_createProc 'db_model_generate_data_types'
GO

ALTER PROCEDURE dbm.db_model_generate_data_types
(
	@schname sysname,
	@source bit, -- 0 - database, 1 - saved objects
	@output smallint, -- 0 - table, 1 - console, 2 - execute 
	@name sysname = NULL
)
AS
BEGIN

/*
EXEC dbm.db_model_save_data_types 
DROP TABLE dbm.db_model_saved_data_types

EXEC dbm.db_model_generate_data_types 'dbo', 1, 0, NULL
EXEC dbm.db_model_generate_data_types 'dbo', 0, 2, 'AllowedOperationsProfileKind'
EXEC dbm.db_model_generate_data_types 'dbo', 0, 1, NULL

SELECT * FROM dbm.db_model_get_data_types
*/
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF OBJECT_ID('tempdb..#result') IS NOT NULL
	BEGIN
		DROP TABLE #result
	END
	
	CREATE TABLE #result
	(
		[row] numeric(10) identity(1,1),
		[batchend] bit,
		[code] varchar(MAX),
	)

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1,1),
		[schname] sysname NULL,	
		[utname] sysname NULL,
		[stname] sysname NULL,
		[nullable] varchar(10) NULL
	)

	IF @source = 0
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[utname],
			[stname],
			[nullable]
		)
			SELECT
				DISTINCT
				G.[schname],
				G.[utname],
				G.[stname],
				CASE
					WHEN G.[nullable] ='Y' THEN 'NULL'
					WHEN G.[nullable] ='N' THEN 'NOT NULL'
				END
			FROM	dbm.db_model_get_data_types AS G
			WHERE	(G.utname = @name
				OR @name IS NULL)
				AND G.schname = @schname
			ORDER BY
				G.[utname]
	END
	ELSE IF @source = 1 AND dbm.db_model_exists_table_column('dbm', 'db_model_saved_data_types', NULL) = 1
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[utname],
			[stname],
			[nullable]
		)
			SELECT
				DISTINCT
				G.[schname],
				G.[utname],
				G.[stname],
				CASE
					WHEN G.[nullable] ='Y' THEN 'NULL'
					WHEN G.[nullable] ='N' THEN 'NOT NULL'
				END
			FROM	dbm.db_model_saved_data_types AS G
			WHERE	(G.utname = @name
				OR @name IS NULL)
				AND G.schname = @schname
			ORDER BY
				G.[utname]
	END
	
	DECLARE @ischname sysname
	DECLARE @iutname varchar(300)
	DECLARE @istname varchar(300)
	DECLARE @inullable varchar(10)
	DECLARE @tmpResult varchar(8000) = ''
	DECLARE @nl varchar(2) = CHAR(13) + CHAR(10)

	DECLARE generateObjects CURSOR FAST_FORWARD FOR
		SELECT
			O.schname,
			O.[utname],
			O.[stname],
			O.[nullable]
		FROM	#db_objects AS O
		ORDER BY
			O.[utname]
	
	OPEN generateObjects

	FETCH NEXT FROM generateObjects INTO @ischname, @iutname, @istname, @inullable
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @tmpResult = ''
		SET @tmpResult = @tmpResult + 'IF dbm.db_model_exists_data_type(''' + @ischname + ''', ''' + @iutname + ''') = 0' + @nl
		SET @tmpResult = @tmpResult + 'BEGIN' + @nl
		SET @tmpResult = @tmpResult + '	CREATE TYPE [' + @ischname + '].[' + @iutname + '] FROM ' + @istname + ' ' + @inullable + @nl
		SET @tmpResult = @tmpResult + 'END' + @nl
		INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
		FETCH NEXT FROM generateObjects INTO @ischname, @iutname, @istname, @inullable
	END

	IF @output = 0
	BEGIN
		SELECT	*
		FROM	#result
	END
	ELSE IF @output = 1
	BEGIN
		EXEC dbm.db_model_print_output '#result'
	END
	ELSE IF @output = 2
	BEGIN
		EXEC dbm.db_model_execute_output '#result'
	END
		
	CLOSE generateObjects
	DEALLOCATE generateObjects
end
