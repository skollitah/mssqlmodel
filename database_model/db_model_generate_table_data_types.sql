EXEC dbm.db_model_utl_createProc 'db_model_generate_table_data_types'
GO

ALTER PROCEDURE dbm.db_model_generate_table_data_types
(	
	@schname sysname,
	@source bit, -- 0 - database, 1 - saved objects
	@output smallint, -- 0 - table, 1 - console, 2 - execute 
	@name sysname = NULL
)
AS BEGIN

/*
EXEC dbm.db_model_save_table_data_types
DROP TABLE dbm.db_model_saved_table_data_types

EXEC dbm.db_model_generate_table_data_types 'dbo', 1, 2
EXEC dbm.db_model_generate_table_data_types 'dbo', 0, 0
EXEC dbm.db_model_generate_table_data_types 'dbm', 0, 1
EXEC dbm.db_model_generate_table_data_types 'dbo', 0, 1, 'TableOfColumns'

SELECT * FROM dbm.db_model_get_table_data_types
*/

	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF

	IF OBJECT_ID('tempdb..#result') IS NOT NULL
	BEGIN
		DROP TABLE #result
	END
	
	CREATE TABLE #result
	(
		[row] numeric(10) identity(1,1),
		[batchend] bit,
		[code] varchar(MAX),
	)	

	IF OBJECT_ID('tempdb..#db_objects') IS NOT NULL
	BEGIN
		DROP TABLE #db_objects
	END

	CREATE TABLE #db_objects (
		[id] numeric(10) identity(1,1),
		[schname] sysname NULL,
		[kname] sysname NULL,
		[cname] sysname NULL,
		[tname] sysname NULL,
		[keyno] smallint NULL,
		[nullable] varchar(20) NULL,
		[collation] sysname NULL
	)

	IF @source = 0
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[kname],
			[cname],
			[tname],
			[keyno],
			[nullable],
			[collation]
		)
			SELECT
				G.[schname],
				G.[tname],
				G.[cname],
				CASE	
					WHEN G.[stname] IS NULL THEN G.[utname]
					ELSE '[' + G.[utname] + ']'
				END,
				G.[colorder],
				CASE
					WHEN G.[nullable] = 'Y' THEN 'NULL'
					WHEN G.[nullable] = 'N' THEN 'NOT NULL'
				END,
				'COLLATE ' + G.collation
			FROM	dbm.db_model_get_table_data_types AS G
			WHERE	(G.tname = @name
				OR @name IS NULL)
				AND G.schname = @schname
			ORDER BY
				G.[tname],
				G.[colorder]
	END
	ELSE IF @source = 1 AND dbm.db_model_exists_table_column('dbm', 'db_model_saved_table_data_types', NULL) = 1
	BEGIN
		INSERT INTO #db_objects (
			[schname],
			[kname],
			[cname],
			[tname],
			[keyno],
			[nullable],
			[collation]
		)
			SELECT
				G.[schname],
				G.[tname],
				G.[cname],
				CASE	
					WHEN G.[stname] IS NULL THEN G.[utname]
					ELSE '[' + G.[utname] + ']'
				END,
				G.[colorder],
				CASE
					WHEN G.[nullable] = 'Y' THEN 'NULL'
					WHEN G.[nullable] = 'N' THEN 'NOT NULL'
				END,
				'COLLATE ' + G.collation
			FROM	dbm.db_model_saved_table_data_types AS G
			WHERE	(G.tname = @name
				OR @name IS NULL)
				AND G.schname = @schname
			ORDER BY
				G.[tname],
				G.[colorder]
	END
	
	DECLARE @ischname sysname
	DECLARE @ikname sysname
	DECLARE @icname sysname
	DECLARE @itname sysname
	DECLARE @ikeyno smallint
	DECLARE @imaxkeyno smallint	
	DECLARE @inullable varchar(20)
	DECLARE @icollation sysname
	DECLARE @tmpResult varchar(8000) = ''
	DECLARE @nl varchar(2) = CHAR(13) + CHAR(10)

	DECLARE generateObjects CURSOR FAST_FORWARD FOR
		SELECT
			DISTINCT
			O.schname,
			O.[kname]
		FROM	#db_objects AS O
		ORDER BY
			O.[kname]
	
	OPEN generateObjects
	
	FETCH NEXT FROM generateObjects INTO @ischname, @ikname
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @tmpResult = ''
		SET @tmpResult = @tmpResult +  'IF dbm.db_model_exists_table_data_type(''' + @ischname + ''', ''' + @ikname + ''', DEFAULT) = 0' + @nl
		SET @tmpResult = @tmpResult +  'BEGIN' + @nl
		SET @tmpResult = @tmpResult +  '	CREATE TYPE [' + @ischname + '].[' + @ikname + '] AS TABLE' + @nl
		SET @tmpResult = @tmpResult +  '	(' + @nl
		
		SET @imaxkeyno =
		(
			SELECT	MAX(O.keyno)
			FROM	#db_objects AS O
			WHERE	O.[kname] = @ikname
		)
		
		DECLARE generateColumns CURSOR FAST_FORWARD FOR
			SELECT
				O.[cname],
				O.[tname],
				O.[keyno],
				O.[nullable],
				O.[collation]				
			FROM	#db_objects AS O
			WHERE	O.kname = @ikname
				AND O.schname = @ischname
			ORDER BY
				O.[keyno]
		
		OPEN generateColumns
		
		FETCH NEXT FROM generateColumns INTO @icname, @itname, @ikeyno, @inullable, @icollation
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @tmpResult = @tmpResult + '		[' + @icname + '] ' + @itname + ISNULL(' ' + @icollation + ' ', ' ') + @inullable 
			IF @ikeyno = @imaxkeyno
			BEGIN
				SET @tmpResult = @tmpResult + @nl
			END
			ELSE
			BEGIN
				SET @tmpResult = @tmpResult + ',' + @nl
			END
			FETCH NEXT FROM generateColumns INTO @icname, @itname, @ikeyno, @inullable, @icollation
		END

		CLOSE generateColumns
		DEALLOCATE generateColumns

		SET @tmpResult = @tmpResult +  '	)' + @nl
		SET @tmpResult = @tmpResult +  'END' + @nl	
		INSERT INTO #result(batchend, code) VALUES(1, @tmpResult)
		
		FETCH NEXT FROM generateObjects INTO @ischname, @ikname
	END
	
	IF @output = 0
	BEGIN
		SELECT	*
		FROM	#result
	END
	ELSE IF @output = 1
	BEGIN
		EXEC dbm.db_model_print_output '#result'
	END
	ELSE IF @output = 2
	BEGIN
		EXEC dbm.db_model_execute_output '#result'
	END
		
	CLOSE generateObjects
	DEALLOCATE generateObjects	
	
END

